<?php

namespace siga\Modelo\insumo\insumo_registros;

use Illuminate\Database\Eloquent\Model;

class EvaluacionProveedor extends Model
{
    protected $table = 'insumo.evaluacion_proveedor';

    protected $fillable = [
        'eval_id',
        'eval_prov_id',
        'eval_evaluacion',
        'enval_registrado',
    ];

    protected $primaryKey = 'eval_id';

    public $timestamps = false;

    protected static function getListar($id_proveedor)
    {	
    	$evaluaciones = EvaluacionProveedor::where('eval_prov_id',$id_proveedor)->get();	
       	return $evaluaciones;
    }

}
