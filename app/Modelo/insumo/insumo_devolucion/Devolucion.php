<?php

namespace siga\Modelo\insumo\insumo_devolucion;

use Illuminate\Database\Eloquent\Model;
use Auth;


class Devolucion extends Model
{
    protected $table = 'insumo.devolucion';

    protected $fillable = [
        'dev_id',
        'dev_id_aprsol',
        'dev_num_sal',
        'dev_data',
        'dev_obs',
        'dev_id_planta',
        'dev_gestion',
        'dev_codnum',
        'dev_usr_id',
        'dev_registrado',
        'dev_modificado',
        'dev_estado',
        'dev_nom_rec',
    ];

    protected $primaryKey = 'dev_id';

    public $timestamps = false;

    protected static function getListar()
    {
        $user=Auth::user()->usr_id;
        $devolucion = Devolucion::where('dev_estado', 'A')
                    ->where('dev_usr_id', $user)
                    ->get();
        return $devolucion;
    }

    protected static function setBuscar($id) {
        $devolucion = \DB::table('insumo.devolucion')
               ->join('public._bp_usuarios', 'insumo.devolucion.dev_usr_id', '=', 'usr_id')
               ->join('public._bp_personas', '_bp_usuarios.usr_prs_id', '=', 'public._bp_personas.prs_id')
               ->where('dev_id',$id)
               ->first();
       return $devolucion;
    }
}
