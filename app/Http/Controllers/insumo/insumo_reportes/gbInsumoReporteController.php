<?php

namespace siga\Http\Controllers\insumo\insumo_reportes;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\insumo\Stock_Almacen;
USE siga\Modelo\insumo\Stock_Almacen_Historial;
use siga\Modelo\admin\Usuario;
use siga\Modelo\insumo\insumo_registros\Insumo;
use siga\Modelo\insumo\insumo_registros\CarroIngreso;
use siga\Modelo\insumo\insumo_registros\DetalleIngresoData;
use siga\Modelo\insumo\insumo_solicitud\Aprobacion_Solicitud;
use siga\Modelo\insumo\insumo_solicitud\DetalleAprobSol;
use siga\Modelo\insumo\Stock_Historial;
use siga\Modelo\insumo\insumo_solicitud\Solicitud;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use Auth;
use PDF;
use TCPDF;
use Illuminate\Support\Facades\Input;

class gbInsumoReporteController extends Controller
{
     public function index()
    {   

        // $dia = 1;
        $time = time();
        $dia =  date("H:i:s", $time);
        // dd($dia);
        if($dia == "21:41:00")
        {
           dd($dia);
        } else {  
            return view('backend.administracion.insumo.insumo_reportes.insumo.index');
        }
    }

     public function create()   
    {
        // $ins = Insumo::all();
        $ins = Insumo::join('insumo.dato as dat','insumo.insumo.ins_id_tip_ins', '=', 'dat.dat_id')
                  ->join('insumo.dato as dat1','insumo.insumo.ins_id_uni', '=', 'dat1.dat_id')
                  ->join('insumo.dato as dat2','insumo.insumo.ins_id_cat', '=', 'dat2.dat_id')
                  ->join('insumo.stock_almacen as stock','insumo.ins_id','=','stock.stockal_ins_id')
                  ->select('insumo.ins_id','insumo.ins_codigo','dat.dat_nom as insumo','dat1.dat_nom as unidad','dat2.dat_nom as categoria','insumo.ins_estado', 'insumo.ins_desc', 'stock.stockal_cantidad')
                  ->where('insumo.ins_estado', 'A')
                 // ->where('ins_usr_id', $usr)
                  // ->where('ins_id_planta', $id)
                  ->get();
        return Datatables::of($ins)->addColumn('acciones', function ($ins) {
            return '<a value="' . $ins->ins_id . '" class="btn btn-success btn-xs" href="/RpKardexInsumo/' . $ins->ins_id . '" type="button" >REPORTE</a>';
        })
            ->editColumn('id', 'ID: {{$ins_id}}')
            ->make(true);
    }

    public function menuInventarioGeneral()
    {   
       $stockalhist = Stock_Historial::join('insumo.insumo as ins','insumo.stock_historial.his_stock_ins_id','=','ins.ins_id')
                                      ->join('insumo.unidad_medida as unidad','ins.ins_id_uni','=','unidad.umed_id')
                                      //->join('insumo.categoria as cat','ins.ins_id_cat','=','cat.cat_id')
                                      ->join('insumo.partida as part','ins.ins_id_part','=','part.part_id')
                                      ->join('public._bp_planta as plan','insumo.stock_historial.his_stock_planta_id','=','plan.id_planta')
                                      /*->select('ins.ins_codigo','ins.ins_desc','unidad.umed_nombre as unidad','part.part_nombre','stock_historial.his_stock_registrado','plan.nombre_planta', DB::raw('sum(stock_historial.his_stock_cant) as quantity'))*/
                                     // ->where('his_stock_registrado','=',$anio."-".$mes."-".$dia)
                                      ->where('his_stock_registrado','2019-07-30')
                                    //  ->groupBy('ins.ins_codigo', 'ins.ins_desc', 'unidad', 'part.part_nombre', 'stock_historial.his_stock_registrado', 'plan.nombre_planta')
                                      ->get();
        echo $stockalhist;
        return view('backend.administracion.insumo.insumo_reportes.inventario_general.index');
    }
    public function ListarInventarioGeneral()
    {   
        $stockalhist = Stock_Historial::join('insumo.insumo as ins','insumo.stock_historial.his_stock_ins_id','=','ins.ins_id')
                                      ->join('insumo.unidad_medida as unidad','ins.ins_id_uni','=','unidad.umed_id')
                                      ->join('insumo.categoria as cat','ins.ins_id_cat','=','cat.cat_id')
                                      ->join('insumo.partida as part','ins.ins_id_part','=','part.part_id')
                                      ->join('public._bp_planta as plan','insumo.stock_historial.his_stock_planta_id','=','plan.id_planta')
                                      ->select('ins.ins_codigo','ins.ins_desc','unidad.umed_nombre as unidad','stock_historial.his_stock_cant','cat.cat_nombre as categoria','part.part_nombre','stock_historial.his_stock_registrado','stock_historial.his_stock_planta_id')
                                      ->get();
        return Datatables::of($stockalhist)
    
        //->editColumn('id', 'ID: {{$carr_sol_id}}') 
        ->make(true);
    }
    // BUSQUEDA POR MES
    public function ReporteInvMes($mes, $anio)
    {
        $anio1 = $anio;        
        $diafinal = date("d", mktime(0, 0, 0, $mes + 1, 0, $anio1));
        $fechainicial = $anio1 . "-" . $mes . "-01";
        $fechafinal = $anio1 . "-" . $mes . "-" . $diafinal;
        $stockalhist = Stock_Historial::join('insumo.insumo as ins','insumo.stock_historial.his_stock_ins_id','=','ins.ins_id')
                                      ->join('insumo.unidad_medida as unidad','ins.ins_id_uni','=','unidad.umed_id')
                                     // ->join('insumo.categoria as cat','ins.ins_id_cat','=','cat.cat_id')
                                      ->join('insumo.partida as part','ins.ins_id_part','=','part.part_id')
                                      ->join('public._bp_planta as plan','insumo.stock_historial.his_stock_planta_id','=','plan.id_planta')
                                      ->select('ins.ins_codigo','ins.ins_desc','unidad.umed_nombre as unidad','part.part_nombre','stock_historial.his_stock_registrado','plan.nombre_planta', DB::raw('sum(stock_historial.his_stock_cant) as quantity'))
                                      ->where('his_stock_registrado','>=',$fechainicial)->where('his_stock_registrado','<=',$fechafinal)
                                      ->groupBy('ins.ins_codigo', 'ins.ins_desc', 'unidad', 'part.part_nombre', 'stock_historial.his_stock_registrado', 'plan.nombre_planta')
                                      ->get(); 
        return Datatables::of($stockalhist)
        ->make(true); 
    }
    // BUSQUEDA POR dia
    public function ReporteInvDia($dia,$mes,$anio)
    {
       $dia = $anio . "-" . $mes . "-". $dia;
       $stockalhist = Stock_Historial::join('insumo.insumo as ins','insumo.stock_historial.his_stock_ins_id','=','ins.ins_id')
                                      ->join('insumo.unidad_medida as unidad','ins.ins_id_uni','=','unidad.umed_id')
                                      //->join('insumo.categoria as cat','ins.ins_id_cat','=','cat.cat_id')
                                      ->join('insumo.partida as part','ins.ins_id_part','=','part.part_id')
                                      ->join('public._bp_planta as plan','insumo.stock_historial.his_stock_planta_id','=','plan.id_planta')
                                      ->select('ins.ins_codigo','ins.ins_desc','unidad.umed_nombre as unidad','part.part_nombre','stock_historial.his_stock_registrado','plan.nombre_planta', DB::raw('sum(stock_historial.his_stock_cant) as quantity'))
                                      ->where(DB::raw('cast(stock_historial.his_stock_registrado as date)'),'=',$dia)
                                      ->groupBy('ins.ins_codigo', 'ins.ins_desc', 'unidad', 'part.part_nombre', 'stock_historial.his_stock_registrado', 'plan.nombre_planta')
                                      ->get();
                                     // ->where(DB::raw('cast(stocks.created_at as date)'),'=',$dia)
        // dd($stockalhist3);       
        return Datatables::of($stockalhist)
        ->make(true);
    }
    // BUSQUEDA POR RANGO
    public function ReporteInvRango($dia_inicio,$mes_inicio,$anio_inicio,$dia_fin,$mes_fin,$anio_fin)
    {
        // dd($anio."-".$mes."-".$dia);
        $fechainicial = $anio_inicio . "-" . $mes_inicio . "-". $dia_inicio;
        $fechafinal = $anio_fin . "-" . $mes_fin . "-" . $dia_fin;
        $stockalhist = Stock_Historial::join('insumo.insumo as ins','insumo.stock_historial.his_stock_ins_id','=','ins.ins_id')
                                      ->leftjoin('insumo.unidad_medida as unidad','ins.ins_id_uni','=','unidad.umed_id')
                                      ->join('insumo.partida as part','ins.ins_id_part','=','part.part_id')
                                      ->join('public._bp_planta as plan','insumo.stock_historial.his_stock_planta_id','=','plan.id_planta')
                                      ->select('ins.ins_codigo','ins.ins_desc','unidad.umed_nombre as unidad','part.part_nombre','stock_historial.his_stock_registrado','plan.nombre_planta', DB::raw('sum(stock_historial.his_stock_cant) as quantity'))
                                      ->where(DB::raw('cast(stock_historial.his_stock_registrado as date)'),'>=',$fechainicial)
                                      ->where(DB::raw('cast(stock_historial.his_stock_registrado as date)'),'<=',$fechafinal)
                                      ->groupBy('ins.ins_codigo', 'ins.ins_desc', 'unidad', 'part.part_nombre', 'stock_historial.his_stock_registrado', 'plan.nombre_planta')
                                      ->get();       
        // dd($stockalhist3);       
        return Datatables::of($stockalhist)
        ->make(true);
    }
    public function rptInventarioGeneral()
    {
    	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
          $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
          PDF::SetXY(120, 204);
          PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('L', 'Carta');
        //PDF::AddPage();

        // create some HTML content
        $usr = Usuario::setNombreRegistro();
        $per=Collect($usr);
        $id =  Auth::user()->usr_id;
      //  echo $id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();

        $reg = Stock_Almacen::join('insumo.insumo as insu','insumo.stock_almacen.stockal_ins_id','=','insu.ins_id')
        					->join('insumo.dato as um','insu.ins_id_uni','=','um.dat_id')
        					->join('insumo.partida as part','insu.ins_id_part','=','part.part_id')
        					->join('insumo.dato as cat','insu.ins_id_cat','=','cat.dat_id')
        					->select('ins_codigo','stockal_cantidad','ins_desc','um.dat_nom as nom_unidad', 'part.part_nom','cat.dat_nom as nom_categoria')
        					->where('stockal_planta_id','=',$planta->id_planta)
                            ->orderby('stockal_id','ASC')->get();
        // dd($reg);
       // echo $reg;
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><br><br><img src="/img/logopeqe.png" width="140" height="65"></th>
                             <th  width="830"><h3 align="center"><br>REPORTE INVENTARIO GENERAL<br>ALMACEN INSUMOS <br>Fecha de Emision: '.date('d/m/Y').'<br></h3></th>
                        </tr>
                    </table>
                    <br><br>
                        <label>GENERADO POR: '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</label>
                        <br><br>
                    <table border="1" cellspacing="0" cellpadding="1">
                     
                        <tr>
                            <th align="center" bgcolor="#3498DB" width="120"><strong>CODIGO</strong></th>
                            <th align="center" bgcolor="#3498DB" width="260"><strong>DETALLE ARTICULO</strong></th>
                            <th align="center" bgcolor="#3498DB" width="120"><strong>UNIDAD</strong></th>
                            <th align="center" bgcolor="#3498DB" width="120"><strong>CANTIDAD</strong></th>
                            <th align="center" bgcolor="#3498DB" width="175"><strong>CATEGORIA</strong></th>
                            <th align="center" bgcolor="#3498DB" width="180"><strong>PARTIDA</strong></th>                            
                        </tr>';
                        $nro=0;
                        $totalCantidad = 0;
                    foreach ($reg as $key => $r) {
                    	$totalCantidad = $totalCantidad + $r->stockal_cantidad;                            
                        $html = $html .'<tr align="center" BGCOLOR="#f3f0ff">
                        		        	<td align="center">'.$r->ins_codigo.'</td>
                                			<td align="center">'.$r->ins_desc.'</td>
                                			<td align="center">'.$r->nom_unidad.'</td>
                                			<td align="center">'.number_format($r->stockal_cantidad,2,'.',',').'</td>
                                			<td align="center">'.$r->nom_categoria.'</td>
                                			<td align="center">'.$r->part_nom.'</td>
                                		</tr>';               
                     }
                     $html = $html .'<tr align="center" BGCOLOR="#f3f0ff">
                        		        	<td align="center" colspan="3">TOTALES</td>
                                			<td align="center">'.number_format($totalCantidad,2,'.',',').'</td>
                                			<td align="center"></td>
                                			<td align="center"></td>
                                	</tr>'; 

                       

                    $htmltable = $html . '</table>';
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_salidas_Almacen.pdf', 'I');
    }

    public function rptKardexInsumo($rep)
    {
    	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
          $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
          PDF::SetXY(120, 204);
          PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('P', 'Carta');
        //PDF::AddPage();

        // create some HTML content
        //$usr = Usuario::setNombreRegistro();
        $id_usuario =  Auth::user()->usr_id;
        $usr = Usuario::join('public._bp_personas as persona','public._bp_usuarios.usr_prs_id','=','persona.prs_id')
                      ->where('usr_id',$id_usuario)->first();
        $per=Collect($usr);
        $id =  Auth::user()->usr_id;

      //  echo $id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();
        
        $insumo = HistoStock::join('insumo.insumo as ins', 'insumo.stock_historial.hist_id_ins', '=', 'ins.ins_id')
                            ->join('insumo.dato as un_medi', 'ins.ins_id_uni','=', 'un_medi.dat_id')
                            ->where('hist_id_planta','=',$planta->id_planta)->where('hist_id_ins', $rep)->orderby('hist_id','ASC')->first();
        // dd($insumo);
        $tabkarde = HistoStock::where('hist_id_planta','=',$planta->id_planta)->where('hist_id_ins', $rep)->get();
        //echo $tabkarde;
               
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><br><br><img src="img/logopeqe.png" width="140" height="65"></th>
                             <th  width="525"><h3 align="center"><br>ALMACEN - '.$planta->nombre_planta.'<br>Fecha Emisión: '.date('d/m/Y').'<br>KARDEX VALORADO</h3>
                             </th>
                        </tr>
                    </table>
                    <br><br>
                        <label>Articulo: '.$insumo->ins_desc.' </label>
                        <br>
                        <label>Unidad de Medida: '.$insumo->dat_nom.'</label>
                        <br><br>
                    <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="35">No.</th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="60"><strong>Fecha</strong></th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="120"><strong>Detalle</strong></th>
                            <th colspan="3" align="center" bgcolor="#3498DB" width="150"><strong>Entrada.</strong></th>
                            <th colspan="3" align="center" bgcolor="#3498DB" width="150"><strong>Salida</strong></th>
                            <th colspan="3" align="center" bgcolor="#3498DB" width="150"><strong>Saldo</strong></th>                    
                        </tr>
                        <tr>                 
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Cant.</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Cost. U.</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Total</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Cant.</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Cost. U.</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Total</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Cant.</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Cost. U.</strong></th>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Total</strong></th>                            
                        </tr>';
                       $nro=0;


                            //$html = $html .'<tr align="center" BGCOLOR="#f3f0ff">
                            //                    <td align="center">'.$nro.'</td>
                            //                    <td align="center">18-01-2019</td>
                            //                    <td align="center">Inventario Inicial</td>
                            //                    <td align="center">0.00</td>
                            //                    <td align="center">0.00</td>
                            //                    <td align="center">0.00</td>
                            //                    <td align="center">0.00</td>
                            //                    <td align="center">0.00</td>
                            //                    <td align="center">0.00</td>
                            //                    <td align="center">160.00</td>
                            //                    <td align="center">0,00</td>
                            //                    <td align="center">0.00</td>
                            //                </tr>';
                            $cant=0;
                    foreach ($tabkarde as $key => $ig) {  
                        $nro = $nro + 1;                    
                            if ($ig->hist_cant_sal == null) {
                                $var = $ig->hist_cant_ent;
                                $cant=$var;
                                //   $cant1=$cant+$ig->hist_cant_ent;
                                $tot=$cant*$ig->hist_cost_ent;
                                $html = $html .   '<tr align="center" BGCOLOR="#f3f0ff">
                                                <td align="center">'.$nro.'</td>
                                                <td align="center">'.$ig->hist_registrado.'</td>
                                                <td align="center">Entrada (NE-'.$ig->hist_detale.')</td>
                                                <td align="center">'.number_format($ig->hist_cant_ent,2,'.',',').'</td>
                                                <td align="center">'.number_format($ig->hist_cost_ent,2,'.',',').'</td>
                                                <td align="center">'.number_format($ig->hist_cant_ent*$ig->hist_cost_ent,2,'.',',').'</td>
                                                <td align="center">0.00</td>
                                                <td align="center">0.00</td>
                                                <td align="center">0.00</td>
                                                <td align="center">'.number_format($cant,2,'.',',').'</td>
                                                <td align="center">'.number_format($ig->hist_cost_ent,2,'.',',').'</td>
                                                <td align="center">'.number_format($tot,2,'.',',').'</td>
                                            </tr>';
                            } elseif($ig->hist_cant_sal > 0) {
                                // dd($var);
                                $cant1=$cant-$ig->hist_cant_sal;
                                // $cant1=$cant+$ig->hist_cant_sal;
                                $cant=$cant1;
                                $tot=$cant*$ig->hist_cost_sal;
                                $html = $html .   '<tr align="center" BGCOLOR="#f3f0ff">
                                                <td align="center">'.$nro.'</td>
                                                <td align="center">'.$ig->hist_registrado.'</td>
                                                <td align="center">'.$ig->hist_detale.'</td>
                                                <td align="center">0.00</td>
                                                <td align="center">0.00</td>
                                                <td align="center">0.00</td>
                                                <td align="center">'.number_format($ig->hist_cant_sal,2,'.',',').'</td>
                                                <td align="center">'.number_format($ig->hist_cost_sal,2,'.',',').'</td>
                                                <td align="center">'.number_format($ig->hist_cant_sal*$ig->hist_cost_sal,2,'.',',').'</td>
                                                <td align="center">'.number_format($cant,2,'.',',').'</td>
                                                <td align="center">'.number_format($ig->hist_cost_sal,2,'.',',').'</td>
                                                <td align="center">'.number_format($tot,2,'.',',').'</td>
                                            </tr>';
                            }             
                    }
                     

                       

                    $htmltable = $html . '</table>
                    <br><br><br><br><br><br><br><br><br><br><br><br>';
                    $htmltable = $htmltable . '
                                <table>
                                    <tr>
                                        <td align="left">Revisado por: __________________________________</td>
                                        
                                        <td align="left">Verificado por: __________________________________</td>
                                    </tr>
                                    <tr>
                                        <td align="left">Firma</td>
                                        
                                        <td align="left">Firma</td>
                                    </tr>
                                    <tr>
                                        <td align="left"></td>
                                        
                                        <td align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Nombre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</td>
                                        <td align="left">Nombre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________________________</td>
                                    </tr>                   
                                </table>';
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_Kardex_Insumo_Almacen.pdf', 'I');
    }

    public function rptMensual()
    {

    	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
          $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
          PDF::SetXY(120, 204);
          PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('L', 'Carta');
        //PDF::AddPage();

        // create some HTML content
        //$usr = Usuario::setNombreRegistro();
        $id_usuario =  Auth::user()->usr_id;
        $usr = Usuario::join('public._bp_personas as persona','public._bp_usuarios.usr_prs_id','=','persona.prs_id')
                      ->where('usr_id',$id_usuario)->first();
        $per=Collect($usr);
        $id =  Auth::user()->usr_id;
      //  echo $id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();
        
        //$insumo_ingreso = CarroIngreso::where('carr_ing_id_planta','=',$planta->id_planta)->get();
        $insumo_ingreso = DetalleIngresoData::where('detcar_id_planta','=',$planta->id_planta)->orderby('detcar_id_ins','ASC')->get();
        //dd($insumo_ingreso);       
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><br><br><img src="img/logopeqe.png" width="140" height="65"></th>
                             <th  width="830"><h3 align="center"><br>REPORTE MENSUAL - '.$planta->nombre_planta.'<br>Fecha Emisión: '.date('d/m/Y').'<br>(EXPRESADO EN BOLIVIANOS)</h3></th>
                        </tr>
                    </table>
                    <br><br>
                        <label><strong>GENERADO POR:</strong> '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</label>
                        <br><br>
                    <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="40">No.</th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="60"><strong>Codigo</strong></th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="220"><strong>Detalle Articulo</strong></th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="70"><strong>Precio U.</strong></th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="70"><strong>Total</strong></th>
                            <th rowspan="2" align="center" bgcolor="#3498DB" width="90"><strong>Unidad</strong></th>
                            <th colspan="3" align="center" bgcolor="#3498DB" width="210"><strong>Cantidad Fisica</strong></th>
                            <th colspan="3" align="center" bgcolor="#3498DB" width="210"><strong>Costo</strong></th>
                             
                        </tr>
                        <tr>                 
                            
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Entrada</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Salida</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Saldo</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Entrada</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Salida</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Saldo</strong></th>                            
                        </tr>';
                        $nro=0;
                        $totalCantidad = 0;

                        $totaCost = 0;
                        $totaSalida = 0;
                        $totaSaldo = 0;
                        //dd($insumo_ingreso);
                    foreach ($insumo_ingreso as $key => $ig) {
                        //dd($ig->carr_ing_id);                     
                        //$carr_data = $ig->carr_ing_data;
                        //$data = json_decode($carr_data); 
                        $nro = $nro + 1;                    
                        $codigo = $this->traeCodigo($ig->detcar_id_ins);
                        $detalle_articulo = $this->traeDetalle($ig->detcar_id_ins);
                        $total = $this->calcularTotal($ig->detcar_costo_uni,$ig->detcar_cantidad);
                        $unidad_medida = $this->traeUnidad($ig->detcar_id_ins);
                        $cantidad_salida = $ig->detcar_cantidad - $ig->detcar_cant_actual;
                        $costo_salidas = $cantidad_salida*$ig->detcar_costo_uni;
                        $saldos_costos = $total - $costo_salidas;

                        $totaCost = $totaCost + $total;
                        $totaSalida = $totaSalida + $costo_salidas;
                        $totaSaldo = $totaSaldo + $saldos_costos;
                        $html = $html .'<tr align="center" BGCOLOR="#f3f0ff">
                                                <td align="center">'.$nro.'</td>
                                                <td align="center">'.$codigo.'</td>
                                                <td align="center">'.$detalle_articulo.'</td>
                                                <td align="center">'.number_format($ig->detcar_costo_uni,2,'.',',').'</td>
                                                <td align="center">'.number_format($total,2,'.',',').'</td>
                                                <td align="center">'.$unidad_medida.'</td>
                                                <td align="center">'.$ig->detcar_cantidad.'</td>
                                                <td align="center">'.$cantidad_salida.'</td>
                                                <td align="center">'.$ig->detcar_cant_actual.'</td>
                                                <td align="center">'.number_format($total,2,'.',',').'</td>
                                                <td align="center">'.number_format($costo_salidas,2,'.',',').'</td>
                                                <td align="center">'.number_format($saldos_costos,2,'.',',').'</td>
                                            </tr>';             
                     }
                     $html = $html .'<tr align="center" BGCOLOR="#f3f0ff">
                                            <td align="center" colspan="9"><strong>TOTALES</strong></td>
                                            
                                            <td align="center"><strong>'.number_format($totaCost,2,'.',',').'</strong></td>
                                            <td align="center"><strong>'.number_format($totaSalida,2,'.',',').'</strong></td>
                                            <td align="center"><strong>'.number_format($totaSaldo,2,'.',',').'</strong></td>                                            
                                    </tr>'; 

                       

                    $htmltable = $html . '</table>';
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_Mensual_Almacen.pdf', 'I');
        
    }

   	public function rptCostoAlmacen()
   	{
   		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS RESUMEN DEL MOVIMIENTO DE COSTOS DE ALMACEN');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
          $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
          PDF::SetXY(120, 204);
          PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('P', 'Carta');
        //PDF::AddPage();

        // create some HTML content
        $usr = Usuario::setNombreRegistro();
        $per=Collect($usr);
        $id =  Auth::user()->usr_id;
      //  echo $id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();
        
        $insumo_ingreso = CarroIngreso::where('carr_ing_id_planta','=',$planta->id_planta)->get();
        $invini = 0;
        foreach ($insumo_ingreso as $key => $insing) {
            $data = json_decode($insing->carr_ing_data);
            foreach ($data as $key => $datin) {
                $inviniStock = $datin->carr_cantidad * $datin->carr_costo;
                $invini = $invini + $inviniStock;
            }            
        }
        $invsal = 0;
        $insumo_salida = Aprobacion_solicitud::where('aprsol_id_planta','=',$planta->id_planta)->get();
        foreach ($insumo_salida as $key => $insal) {
            $datasal = json_decode($insal->aprsol_data);
            foreach ($datasal as $key => $datsal) {
                $preciosal = $this->obtenerPrecio($datsal->id_insumo);
                $invsalStock = $datsal->cantidad * $preciosal;
                $invsal = $invsal + $invsalStock;
            }            
        }
        
        $invdisp = 100000 + $invini; 
        $costconsu = $invdisp - $invsal;      
        $htmltable = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><br><br><img src="img/logopeqe.png" width="140" height="65"></th>
                             <th  width="525"><h3 align="center"><br>CUADRO DE RESUMEN DEL MOVIMIENTO DE COSTOS DE ALMACEN<br>Almacen - '.$planta->nombre_planta.'<br>AL: '.date('d/m/Y').'<br>(Expresado en Bolivianos)</h3>
                             </th>
                        </tr>
                    </table>
                    <br><br><br><br><br>
                    ';
        $htmltable = $htmltable.'
                                <table>
                                    <tr>
                                        <td width="60"></td>
                                        <td width="170">INVENTARIO INICIAL</td>
                                        <td width="60" align="rigth">100000</td>
                                    </tr>
                                    <tr>
                                        <td width="60">mas</td>
                                        <td width="170">COMPRAS</td>
                                        <td width="60" align="rigth" style="border-bottom:1px solid black;">'.$invini.'</td>
                                    </tr>
                                    <tr>
                                        <td width="60"></td>
                                        <td width="170">INVENTARIO DISP.</td>
                                        <td width="60" align="rigth">'.$invdisp.'</td>
                                    </tr>
                                    <tr>
                                        <td width="60">menos</td>
                                        <td width="170">COSTOS CONSUMIDOS</td>
                                        <td width="60" align="rigth" style="border-bottom:1px solid black;">'.$invsal.'</td>
                                    </tr>
                                    <tr>
                                        <td width="60"></td>
                                        <td width="170">COSTO MERCADERIA DISP.</td>
                                        <td width="60" align="rigth">'.$costconsu.'</td>
                                    </tr>
                                </table>
                                ';
        
                    
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_Costo_Almacen.pdf', 'I');   	
    }

    //FUNCIONES
    function calcularTotal($precio,$cantidad)
    {   
        $result = $precio*$cantidad;
        return $result;
    }
    function obtenerSalidas($id_insumo)
    {
        $cantidad_insumo = 0;
        //$salida = Aprobacion_solicitud::where('aprsol_estado','=','A')->get();
        $salida = DetalleAprobSol::where('detaprob_id_ins',$id_insumo)->get();
        //dd($salida);
        foreach ($salida as $key => $sal) {
            //dd($sal);
            //$dato = json_decode($sal->aprsol_data);
            //$dato_insumo_id = $dato[0]->id_insumo;
            //if ($dato_insumo_id == $id_insumo) {
            //    $cantidad_insumo = $cantidad_insumo + ($dato[0]->cantidad+$dato[0]->rango_adicional+$dato[0]->solicitud_adicional); 
            //}
            $cantidad_insumo = $cantidad_insumo + $sal->detaprob_cantidad;
        }
        return $cantidad_insumo;
    }
    function obtenerPrecio($id_insumo)
    {
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',Auth::user()->usr_id)->first();
        $precio_insumo = CarroIngreso::where('carr_ing_id_planta','=',$planta->id_planta)->get();
        $pri = 0;
        foreach ($precio_insumo as $key => $precioins) {
            $datains = json_decode($precioins->carr_ing_data);
            foreach ($datains as $key => $datin) {
                if ($id_insumo == $datin->carr_id_insumo) {
                   $pri = $pri + $datin->carr_costo;
                }                 
            } 
        }
        return $pri;

    }
    function traeCodigo($id_insumo){
        $insumo = Insumo::where('ins_id',$id_insumo)->first();
        return $insumo->ins_codigo;
    }
    function traeDetalle($id_insumo){
        $insumo = Insumo::where('ins_id',$id_insumo)->first();
        return $insumo->ins_desc;
    }
    function traeUnidad($id_insumo){
        $insumo = Insumo::join('insumo.dato as uni','insumo.insumo.ins_id_uni','=','uni.dat_id')
                        ->where('ins_id',$id_insumo)->first();
        return $insumo->dat_nom;
    }

    // LISTAR INGRESO POR ALMACEN
    public function listarIngresoAlmacen()
    {
        $reg = CarroIngreso::join('public._bp_usuarios as usu','insumo.carro_ingreso.carr_ing_usr_id','=','usu.usr_id')
                            ->join('public._bp_personas as persona','usu.usr_prs_id','=','persona.prs_id')
                            ->join('insumo.dato', 'carro_ingreso.carr_ing_tiping', '=', 'insumo.dato.dat_id')
                            ->orderby('carr_ing_id','DESC')->get();
        // dd($reg);
        return view('backend.administracion.insumo.insumo_reportes.ingresos_almacen.index');
    }

    public function createListarIngresoAlmacen()
    {
        $reg = CarroIngreso::join('public._bp_usuarios as usu','insumo.carro_ingreso.carr_ing_usr_id','=','usu.usr_id')
                            ->join('public._bp_personas as persona','usu.usr_prs_id','=','persona.prs_id')
                            ->join('insumo.dato', 'carro_ingreso.carr_ing_tiping', '=', 'insumo.dato.dat_id')
                            ->orderby('carr_ing_id','DESC')->get();
        return Datatables::of($reg)->addColumn('acciones', function ($reg) {
            return '<a value="' . $reg->carr_ing_id . '" target="_blank" class="btn btn-primary" href="/ReporteIngreso/' . $reg->carr_ing_id . '" type="button" ><i class="fa fa-eye"></i> REPORTE</a>';
        })->addColumn('nombre_usuario', function ($usuario) {
                return $usuario->prs_nombres.' '.$usuario->prs_paterno.' '.$usuario->prs_materno;
             })  
            ->editColumn('id', 'ID: {{$carr_ing_id}}')
            ->make(true);
    }
    // LISTAR SOLICITUD POR ALMACEN
    public function listarSolicitudAlmacen()
    {

        return view('backend.administracion.insumo.insumo_reportes.solicitud_almacen.index');
    }
    
    public function listReceta()
    {
        $solReceta = Solicitud::getlistarPlanta();

        return Datatables::of($solReceta)->addColumn('acciones', function ($solReceta) {
            
                return '<div class="text-center"><button value="' . $solReceta->sol_id . '" class="btn btn-success btn-xs" onClick="mostrarSolReceta(this);" data-toggle="modal" data-target="#myCreateSolRecibidas"><i class="fa fa-file"></i> Ver</button></div>';
            
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
             -> addColumn('sol_rec_estado', function ($solReceta) {
                if($solReceta->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solReceta->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solReceta->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
             }) 
            ->addColumn('fecha', function ($fecha) {
                $fechasolrec = Carbon::parse($fecha->sol_registrado);
                $mfecha = $fechasolrec->month;
                $dfecha = $fechasolrec->day;
                $afecha = $fechasolrec->year;
                return $dfecha.'-'.$mfecha.'-'.$afecha;
        })
            -> addColumn('nombresCompletoRe', function ($solReceta) {
            return $solReceta->prs_nombres . ' ' . $solReceta->prs_paterno . ' ' . $solReceta->prs_materno;
        })
            ->make(true);
    }
    public function listMaquila()
    {
        $solMaquila = Solicitud::getlistarTraspasoPlanta();
        return Datatables::of($solMaquila)->addColumn('acciones', function ($solMaquila) {
            
            return '<div class="text-center"><button value="' . $solMaquila->sol_id . '" class="btn btn-success btn-xs" onClick="mostrarSolTraspaso(this);" data-toggle="modal" data-target="#myCreateSolTraspaso"><i class="fa fa-truck"></i> Ver</button><div>';
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
            -> addColumn('sol_maq_estado', function ($solMaquila) {
                if($solMaquila->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solMaquila->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solMaquila->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
            })
             -> addColumn('nombresCompletoMa', function ($solMaquila) {
                return $solMaquila->prs_nombres . ' ' . $solMaquila->prs_paterno . ' ' . $solMaquila->prs_materno;
        })  
            ->make(true);
    }

     public function listAdicional()
    {
        $solAdicinal = Solicitud::getlistarAdiInsumoPlanta();
        return Datatables::of($solAdicinal)->addColumn('acciones', function ($solAdicinal) {
            
            return '<div class="text-center"><button value="' . $solAdicinal->sol_id . '" class="btn btn-success btn-xs" onClick="mostrarSolInsumos(this);" data-toggle="modal" data-target="#myCreateSolInsumos"><i class="fa fa-cube"></i> Ver</button></div>';
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
            -> addColumn('sol_ins_estado', function ($solAdicinal) {
                if($solAdicinal->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solAdicinal->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solAdicinal->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
            })
            -> addColumn('nombresCompletoAdi', function ($solAdicinal) {
                return $solAdicinal->prs_nombres . ' ' . $solAdicinal->prs_paterno . ' ' . $solAdicinal->prs_materno;
        })  
            ->make(true);
    }
    // LISTAR SALIDAS POR ALMACEN
    public function listarSalidasAlmacen()
    {
        //dd("AQUI VA LAS SALIDAS");
        return view('backend.administracion.insumo.insumo_reportes.salidas_almacen.index');
    }
    public function listRecetaSal()
    {
        //$solReceta = Solicitud::getlistarPlanta();
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                            ->where('usr_id','=',Auth::user()->usr_id)->first();
        $solReceta = Solicitud::join('insumo.receta as rec','insumo.solicitud.sol_id_rec','=','rec.rec_id')
                    ->join('public._bp_usuarios as usu','insumo.solicitud.sol_usr_id','=','usu.usr_id')
                    ->join('public._bp_personas as persona','usu.usr_prs_id','=','persona.prs_id')
                    ->leftjoin('insumo.aprobacion_solicitud as aprsol','insumo.solicitud.sol_id','=','aprsol.aprsol_solicitud')
                    ->where('sol_id_planta','=',$planta->id_planta)->where('sol_id_tipo','=',1)
                    ->where('sol_estado','B')
                    ->where('aprsol_estado','A')
            ->orderby('sol_id','DESC')->get();
        return Datatables::of($solReceta)->addColumn('acciones', function ($solReceta) {
            if($solReceta->sol_estado == 'B')
            {
                // return '<h4 class="text"><span class="label label-info">RECIBIDO</span></h4>';
                return '<div class="text-center"><a href="BoletaAprovaReceta/' . $solReceta->aprsol_id . '" class="btn btn-md btn-primary" target="_blank">Boleta Salida <i class="fa fa-file"></i></a></div>';
            }
                // return '<div class="text-center"><button value="' . $solReceta->sol_id . '" class="btn btn-success btn-xs" onClick="mostrarSolReceta(this);" data-toggle="modal" data-target="#myCreateSolRecibidas"><i class="fa fa-file"></i> Ver</button></div>';
            
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
             -> addColumn('sol_rec_estado', function ($solReceta) {
                if($solReceta->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solReceta->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solReceta->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
             }) 
            ->addColumn('fecha', function ($fecha) {
                $fechasolrec = Carbon::parse($fecha->sol_registrado);
                $mfecha = $fechasolrec->month;
                $dfecha = $fechasolrec->day;
                $afecha = $fechasolrec->year;
                return $dfecha.'-'.$mfecha.'-'.$afecha;
        })
            -> addColumn('nombresCompletoRe', function ($solReceta) {
            return $solReceta->prs_nombres . ' ' . $solReceta->prs_paterno . ' ' . $solReceta->prs_materno;
        })
            ->make(true);
    }
    public function listMaquilaSal()
    {
        //$solMaquila = Solicitud::getlistarTraspasoPlanta();
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                            ->where('usr_id','=',Auth::user()->usr_id)->first();
        $solMaquila = Solicitud::join('public._bp_planta as planta','insumo.solicitud.sol_id_origen','=','planta.id_planta')
                    ->join('public._bp_planta as plant','insumo.solicitud.sol_id_destino','=','plant.id_planta')
                    ->join('insumo.insumo as ins','insumo.solicitud.sol_id_insmaq','=','ins.ins_id')
                    ->join('public._bp_usuarios as usu','insumo.solicitud.sol_usr_id','=','usu.usr_id')
                    ->join('public._bp_personas as persona','usu.usr_prs_id','=','persona.prs_id')
                    ->leftjoin('insumo.aprobacion_solicitud as aprsol','insumo.solicitud.sol_id','=','aprsol.aprsol_solicitud')
                    ->select('insumo.solicitud.sol_id','insumo.solicitud.sol_estado','insumo.solicitud.sol_registrado','planta.nombre_planta as planta_origen','plant.nombre_planta as planta_destino','persona.prs_nombres','persona.prs_paterno','persona.prs_materno','insumo.solicitud.sol_codnum','aprsol.aprsol_estado','aprsol_id')
                    ->where('sol_id_planta','=',$planta->id_planta)->where('sol_id_tipo','=',3)
                    ->where('sol_estado','B')
                    ->where('aprsol_estado','A')
            ->get();
        return Datatables::of($solMaquila)->addColumn('acciones', function ($solMaquila) {
            
            // return '<div class="text-center"><button value="' . $solMaquila->sol_id . '" class="btn btn-success btn-xs" onClick="mostrarSolTraspaso(this);" data-toggle="modal" data-target="#myCreateSolTraspaso"><i class="fa fa-truck"></i> Ver</button><div>';
            if ($solMaquila->sol_estado == 'B') {
                // return '<h4 class="text"><span class="label label-info">RECIBIDO</span></h4>';
                return '<div class="text-center"><a href="BoletaAprovaTraspaso/'.$solMaquila->aprsol_id.'" class="btn btn-md btn-primary" target="_blank">Boleta Salida <i class="fa fa-file"></i></a></div>';
            }
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
            -> addColumn('sol_maq_estado', function ($solMaquila) {
                if($solMaquila->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solMaquila->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solMaquila->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
            })
             -> addColumn('nombresCompletoMa', function ($solMaquila) {
                return $solMaquila->prs_nombres . ' ' . $solMaquila->prs_paterno . ' ' . $solMaquila->prs_materno;
        })  
            ->make(true);
    }

     public function listAdicionalSal()
    {
        //$solAdicinal = Solicitud::getlistarAdiInsumoPlanta();
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                            ->where('usr_id','=',Auth::user()->usr_id)->first();
        $solAdicinal = Solicitud::join('insumo.receta as rec','insumo.solicitud.sol_id_rec','=','rec.rec_id')
                    ->join('public._bp_usuarios as usu','insumo.solicitud.sol_usr_id','=','usu.usr_id')
                    ->join('public._bp_personas as persona','usu.usr_prs_id','=','persona.prs_id')
                    ->leftjoin('insumo.aprobacion_solicitud as aprsol','insumo.solicitud.sol_id','=','aprsol.aprsol_solicitud')
                    ->where('sol_id_planta','=',$planta->id_planta)->where('sol_id_tipo','=',2)
                    ->where('sol_estado','B')
                    ->where('aprsol_estado','A')
            ->get();
        return Datatables::of($solAdicinal)->addColumn('acciones', function ($solAdicinal) {
            
            // return '<div class="text-center"><button value="' . $solAdicinal->sol_id . '" class="btn btn-success btn-xs" onClick="mostrarSolInsumos(this);" data-toggle="modal" data-target="#myCreateSolInsumos"><i class="fa fa-cube"></i> Ver</button></div>';
            if ($solAdicinal->sol_estado == 'B') {
                // return '<h4 class="text"><span class="label label-info">RECIBIDO</span></h4>';
                return '<div class="text-center"><a href="BoletaAprovaInsumoAdi/' . $solAdicinal->aprsol_id . '" class="btn btn-md btn-primary" target="_blank">Boleta Salida <i class="fa fa-file"></i></a></div>';
            }
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
            -> addColumn('sol_ins_estado', function ($solAdicinal) {
                if($solAdicinal->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solAdicinal->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solAdicinal->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
            })
            -> addColumn('nombresCompletoAdi', function ($solAdicinal) {
                return $solAdicinal->prs_nombres . ' ' . $solAdicinal->prs_paterno . ' ' . $solAdicinal->prs_materno;
        })  
            ->make(true);
    }

    //REPORTE INGRESO
    public function reporteIngreso($id_ingreso){

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
            $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
            PDF::SetXY(120, 204);
            PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('Carta');
        //PDF::AddPage();

        // create some HTML content
   //     $carr = CarritoSolicitud::getListar();
        $usr = Usuario::setNombreRegistro();
        $per=Collect($usr);
        //echo $per;
        $id =  Auth::user()->usr_id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();
        $reg = CarroIngreso:://join('insumo.proveedor as prov', 'carro_ingreso.carr_ing_prov', '=', 'prov.prov_id')
                            join('insumo.dato', 'carro_ingreso.carr_ing_tiping', '=', 'insumo.dato.dat_id')
                            ->where('carr_ing_id',$id_ingreso)->orderby('carr_ing_id','DESC')->take(1)->first();
        // echo $reg['carr_ing_usr_id'];
        $data = $reg['carr_ing_data'];

        $array = json_decode($data);

        $mesesLiteral = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = Carbon::parse($reg['carr_ing_fech']);
        $mfecha = $fecha->month;
        $mes = $mesesLiteral[($mfecha)-1];
        $dfecha = $fecha->day;
        $afecha = $fecha->year;
    
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><img src="img/logopeqe.png" width="160" height="65"></th>
                             <th  width="370"><h3 align="center"><br>ALMACEN '.$planta['nombre_planta'].'<br>NOTA DE INGRESO</h3></th>
                             <th  width="150"><h3 align="center"><br>Fecha: '.date('d/m/Y').'<br>Codigo No: '.$reg['carr_ing_num'].'/'.$reg['carr_ing_gestion'].'</h3></th>
                        </tr>
                    </table>
                    <br><br><br>
                        <label><strong>Responsable de Almacen:</strong> '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Nro Contrato:</strong> '.$reg['carr_ing_nrocontrato'].'</label>
                        <br><br>
                        <label><strong>Dependencia:</strong> '.$planta['nombre_planta'].'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Tipo:</strong> '.$reg['dat_nom'].'</label>
                        <br><br>
                        <label><strong>Fecha Nota Rem:</strong> '.$dfecha.' de '.$mes.' de '.$afecha.'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Nota Remisión:</strong> '.$reg['carr_ing_rem'].'</label>
                        <br><br>
                    <br><br><br>

                    <table border="1" cellspacing="0" cellpadding="1">
                     
                        <tr>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Nro</strong></th>
                            <th align="center" bgcolor="#3498DB" width="80"><strong>Unidad</strong></th>
                            <th align="center" bgcolor="#3498DB" width="150"><strong>Descripcion</strong></th>
                            
                            <th align="center" bgcolor="#3498DB" width="100"><strong>Proveedor</strong></th>
                            <th align="center" bgcolor="#3498DB" width="90"><strong>Cantidad</strong></th>
                            <th align="center" bgcolor="#3498DB" width="90"><strong>Costo U.</strong></th>
                            <th align="center" bgcolor="#3498DB"><strong>Costo Tot.</strong></th>
                        </tr> ';
                        $nro=0;
                        $tot1=0;
                    //    echo $data;
                    foreach ($array as $d) {
                        $nro = $nro+1; 
                        $tot = $d->carr_cantidad * $d->carr_costo;
                            $html = $html . '<tr align="center" BGCOLOR="#f3f0ff">
                                    <td align="center">'. $nro .'</td>
                                    <td align="center">'. $d->dat_nom .'</td>
                                    <td align="center">'.$d->carr_insumo.'</td>
                                    
                                    <td align="center">'.$d->prov_nom.'</td>
                                    <td align="center">'.number_format($d->carr_cantidad,2,'.',',').'</td>
                                    <td align="center">'.number_format($d->carr_costo,2,'.',',').'</td>
                                    <td align="center">'.number_format($tot,2,'.',',').'</td>
                                  </tr>';  
                        $tot1=$tot1+$tot;
                       // echo $tot1;    

                     }

                      $html = $html . '<tr>
                                <th colspan="6" align="center"><strong>TOTAL:</strong></th>
                                <th align="center"><strong>'.number_format($tot1,2,'.',',').'</strong></th>
                                </tr>
                                <br><br><br><br><br><br>
                                _________________________ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________
                                <br>
                                <label>'. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</label>
                                <br>
                                <label><strong>Responsable de Almacen</strong></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Responsable Administrativo</strong></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Jefe de Planta</strong></label>
                            ';   

                    $htmltable = $html . '</table>';
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_almacen.pdf', 'I');
        //dd("AQUI IRA UN REPORTE");
    }
}
