<?php

namespace siga\Http\Controllers\insumo\insumo_registros;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\insumo\insumo_registros\Proveedor;
use siga\Modelo\insumo\insumo_registros\EvaluacionProveedor;
use siga\Modelo\admin\Usuario;//NEW
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Auth;

class gbProveedorController extends Controller
{
    public function index()
    {
    	return view('backend.administracion.insumo.insumo_registro.proveedores.index');
    }

     public function create()
    {
        $prov = Proveedor::getListar();
        return Datatables::of($prov)->addColumn('acciones', function ($prov) {
            return '<button value="' . $prov->prov_id . '" class="btncirculo btn-xs btn-warning" onClick="MostrarProv(this);" data-toggle="modal" data-target="#myUpdateProv"><i class="fa fa-pencil-square"></i></button><button value="' . $prov->prov_id . '" class="btncirculo btn-xs btn-danger" onClick="Eliminar(this);"><i class="fa fa-trash-o"></i></button>';
        })->addColumn('evaluacion_prov', function ($eval_prov) {
            return '<button value="' . $eval_prov->prov_id . '" class="btn-warning btn-md" onClick="FormEval(this);" data-toggle="modal" data-target="#formEvaluacion"><i class="fa fa-file"></i></button><button value="' . $eval_prov->prov_id . '" class="btn-danger btn-md" data-toggle="modal" data-target="#myListEvaluacion" onClick="MostrarEvaluacion(this);"><i class="fa fa-list-alt"></i></button>';
        })
            ->editColumn('id', 'ID: {{$prov_id}}')
            ->make(true);
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
                'nombre_proveedor' => 'required',
                'nombre_responsable' => 'required',                
        ]); 
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                          ->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        Proveedor::create([
            'prov_nom'        => $request['nombre_proveedor'],
            'prov_dir'        => $request['prov_dir'],
            'prov_tel'        => $request['prov_tel'],
            'prov_nom_res'    => $request['nombre_responsable'],
            'prov_ap_res'     => $request['prov_ap_res'],
            'prov_am_res'     => $request['prov_am_res'],
            'prov_tel_res'    => $request['prov_tel_res'],
            'prov_obs'        => $request['prov_obs'],
            'prov_estado'     => 'A',
            'prov_usr_id'     => Auth::user()->usr_id,
            'prov_id_planta'  => $planta->id_planta,
        ]);
        return response()->json(['Mensaje' => 'Se registro correctamente']);
    }

    public function edit($id)
    {
        $prov = Proveedor::setBuscar($id);
        return response()->json($prov);
    }

    public function update(Request $request, $id)
    {
        $prov = Proveedor::setBuscar($id);
        $prov->fill($request->all());
        $prov->save();
        return response()->json($prov->toArray());
    }

    public function destroy($id)
    {
        $prov = Proveedor::getDestroy($id);
        return response()->json($prov);

    }

    public function storeEvalProv(Request $request)
    {
        EvaluacionProveedor::create([
            'eval_prov_id' => $request['eval_prov_id'],
            'eval_evaluacion' => $request['eval_evaluacion'],
        ]);
        return response()->json(['Mensaje'=>'Se rregistro correctamente']);
    }

    public function listarEvaluaciones($id_proveedor)
    {
        $eval_prov = EvaluacionProveedor::join('insumo.proveedor as prov','insumo.evaluacion_proveedor.eval_prov_id','=','prov.prov_id')
                    ->where('prov_id','=',$id_proveedor)->get();
        return \Response::json($eval_prov);
    }
}
