<?php

namespace siga\Http\Controllers\insumo\insumo_registros;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\acopio\Envio_Almacen;
use siga\Modelo\admin\Usuario;
use siga\Modelo\insumo\insumo_registros\Insumo;
use siga\Modelo\insumo\insumo_registros\Ingreso;
use siga\Modelo\insumo\insumo_registros\DetalleIngreso;
use siga\Modelo\insumo\Stock;
use siga\Modelo\insumo\Stock_Almacen;
use siga\Modelo\HistoStock;
use siga\Modelo\insumo\insumo_registros\MatPrimAprobado;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Auth;
use PDF;
use TCPDF;

class gbIngresoPrimaController extends Controller
{
    public function index()
    {
      $combo = Insumo::join('insumo.tipo_insumo as tipins','insumo.insumo.ins_id_tip_ins','=','tins_id')->select('ins_id', 'ins_desc', 'ins_codigo')->where('tipins.tins_id',3)->get();
    	return view('backend.administracion.insumo.insumo_registro.ingreso_prima.index', compact('combo'));
    }

     public function create()
    {
    	$planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        $id=$planta->id_planta;
    	$envio = \DB::table('acopio.envio_almacen')
               ->join('public._bp_usuarios', 'acopio.envio_almacen.enval_usr_id', '=', 'usr_id')
               ->join('public._bp_personas', '_bp_usuarios.usr_prs_id', '=', 'public._bp_personas.prs_id')
               ->where('enval_id_planta',$id)
               // ->where('enval_estado', 'A')
               ->get();
        return Datatables::of($envio)->addColumn('acciones', function ($envio) {
            if ($envio->enval_estado=='A') {
                return '<div class="text-center"><button value="' . $envio->enval_id . '" id="button" class="btn btn-success insumo-get" onClick="MostrarEnvio(this)" data-toggle="modal" data-target="#myCreatePrima"><i class="fa fa-eye"></i> Detalle</button></div>';
            }elseif($envio->enval_estado=='B'){
                //return '<button value="' . $envio->enval_id . '" id="button" class="btn btn-primary btn-xs insumo-get" onClick="MostrarEnvio(this)" data-toggle="modal" data-target="#myCreatePrima">REPORTE <i class="fa fa-file"><i></button>';
               return '<div class="text-center"><a href="ReportePrimaEnval/' . $envio->enval_id . '" class="btn btn-md btn-primary" target="_blank">Boleta <i class="fa fa-file"></i></a></div>';
            }
            
        })
         ->addColumn('nombre', function ($nombre) {
            return $nombre->prs_nombres . ' ' . $nombre->prs_paterno. ' ' . $nombre->prs_materno;
        })
         // -> addColumn('sol_rec_estado', function ($solReceta) {
         //        if($solReceta->sol_estado=='A')
         //        { 
         //            return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
         //        }elseif($solReceta->aprsol_estado == 'A')
         //        {
         //            return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
         //        }elseif($solReceta->aprsol_estado=='B')
         //        { 
         //            return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
         //        }
                            
         //     }) 
            ->editColumn('id', 'ID: {{$enval_id}}')
            ->make(true);
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
            'obs'       => 'required',
            'unidad'    => 'required',
            'insumo'    => 'required',
            'cantidad'  => 'required',
            'costo'     => 'required',
        ]); 

    	  $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        $id_planta=$planta->id_planta;
        $num = Ingreso::join('public._bp_planta as plant', 'insumo.ingreso.ing_planta_id', '=', 'plant.id_planta')->select(DB::raw('MAX(ing_enumeracion) as nroprim'))->where('plant.id_planta', $id_planta)->first();
        $cont=$num['nroprim'];
        $nid = $cont + 1;
        $fecha=date('Y');
        $materia_prima = Ingreso::create([
            'ing_id_tiping'   => 1,
            'ing_enumeracion' => $nid,
            'ing_usr_id'      => Auth::user()->usr_id,
            'ing_planta_id'   => $id_planta,
            'ing_env_acop_id' => $request['id_enval'],
            'ing_obs'         => $request['obs'],           
        ]);

        $detingreso = DetalleIngreso::create([
            'deting_ins_id'     => $request['insumo'],
            'deting_prov_id'    => 1,
            'deting_cantidad'   => $request['cantidad'],
            'deting_costo'      => $request['costo'],
            'deting_ing_id'     => $materia_prima->ing_id,
        ]);

        Stock::create([
            'stock_ins_id'      => $request['insumo'],
            'stock_deting_id'   => $detingreso->deting_id,
            'stock_cantidad'    => $request['cantidad'],
            'stock_costo'       => $request['costo'],
            'stock_fecha_venc'  => '2019-07-26',
            'stock_planta_id' => $id_planta,
        ]);

        /*$prima = MatPrimAprobado::orderby('prim_id','DESC')->take(1)->get();
        foreach ($prima as $prim) {
        
           $stock1 = Stock_Almacen::where('stockal_ins_id','=' ,$prim->prim_tipo_ins)->where('stockal_planta_id','=',$planta->id_planta)->first();
                if($stock1==null)
                {
                    $fecha1=date('d/m/Y');
                        $cant_ingreso = $prim->prim_cant;
                        $id_insumo = $prim->prim_tipo_ins;
                        $id_plant = $planta->id_planta;
                     Stock_Almacen::create([
                        'stockal_ins_id'    => $id_insumo,
                        'stockal_planta_id' => $id_plant,
                        'stockal_cantidad'  => $cant_ingreso,
                        'stockal_fecha'     => $fecha1,
                        'stockal_estado'    => 'A',
                        'stockal_usr_id'    => Auth::user()->usr_id,
                    ]);
                     $total_ent = $cant_ingreso*$request['costo'];
                     HistoStock::create([
                        'hist_id_carring'     => $materia_prima->prim_id,
                        'hist_id_planta'      => $id_plant,
                        'hist_detale'         => $nid,
                        'hist_cant_ent'       => $cant_ingreso,
                        'hist_cost_ent'       => $request['costo'],
                        'hist_tot_ent'        => $total_ent,
                        'hist_usr_id'         => Auth::user()->usr_id,
                        'hist_id_ins'         => $id_insumo,
                    ]);
                }
                else{

                      $id_ins = $stock1->stockal_ins_id;
                      $id_insumo = $prim->prim_tipo_ins;
                      $id_plant = $planta->id_planta;
                      $cant_ingreso = $prim->prim_cant;
                      $stock_cantidad_ingreso = $stock1->stockal_cantidad + $cant_ingreso;
                      $stock1->stockal_cantidad = $stock_cantidad_ingreso;
                      $stock1->stockal_usr_id = Auth::user()->usr_id;
                      $stock1->save(); 
                      $total_ent = $cant_ingreso*$request['costo'];
                      HistoStock::create([
                        'hist_id_carring'     => $materia_prima->prim_id,
                        'hist_id_planta'      => $id_plant,
                        'hist_detale'         => $nid,
                        'hist_cant_ent'       => $cant_ingreso,
                        'hist_cost_ent'       => $request['costo'],
                        'hist_tot_ent'        => $total_ent,
                        'hist_usr_id'         => Auth::user()->usr_id,
                        'hist_id_ins'         => $id_insumo,
                    ]);                   
                }
        }*/    

        $envioal_matprim = Envio_Almacen::where('enval_id','=',$request['id_enval'])->first();
        $envioal_matprim->enval_estado = 'B';
        $envioal_matprim->save();

        return response()->json($materia_prima);
    }

    public function edit($id)
    {
        $envio = \DB::table('acopio.envio_almacen')
               ->join('public._bp_usuarios', 'acopio.envio_almacen.enval_usr_id', '=', 'usr_id')
               ->join('public._bp_personas', '_bp_usuarios.usr_prs_id', '=', 'public._bp_personas.prs_id')
        	   ->where('enval_id', $id)->first();
        return response()->json($envio);
    }

    public function reportePrima($id_ingreso)
    {
        //dd($id_ingreso);
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
            $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
            PDF::SetXY(120, 204);
            PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('Carta');
        //PDF::AddPage();

        // create some HTML content
   //     $carr = CarritoSolicitud::getListar();
        $usr = Usuario::join('public._bp_personas as per','public._bp_usuarios.usr_prs_id','=','per.prs_id')
                ->where('usr_id',Auth::user()->usr_id)->first();
        $per=Collect($usr);
        //echo $per;
        $id =  Auth::user()->usr_id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();
        $reg = Ingreso::join('acopio.envio_almacen as env','insumo.ingreso.ing_env_acop_id','=','env.enval_id')
                      ->where('ing_id',$id_ingreso)->first();
        $detalle_ingreso = DetalleIngreso::where('deting_ing_id',$id_ingreso)->first();
        // echo $reg['carr_ing_usr_id'];
    
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><img src="img/logopeqe.png" width="160" height="65"></th>
                             <th  width="400"><h3 align="center"><br>ALMACEN '.$planta['nombre_planta'].'<br>NOTA DE INGRESO<br>MATERIA PRIMA<br></h3></th>
                             <th  width="150"><h3 align="center"><br>Fecha: '.date('d/m/Y').'<br>Codigo No:'.$reg['ing_enumeracion'].'/'.date('Y',strtotime($reg['ing_registrado'])).'</h3></th>
                        </tr>
                    </table>
                    <br><br><br>
                        <label><strong>Responsable de Almacen</strong>: '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</label>
                        <br><br>
                        <label><strong>Dependencia:</strong> '.$planta['nombre_planta'].'</label>
                        <br><br>
                        <label><strong>Insumo:</strong> LECHE </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Cantidad Total Enviado:</strong> '.$reg['enval_cant_total'].'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Cantidad Total Recibido:</strong> '.$detalle_ingreso['deting_cantidad'].'</label>
                        <br><br>
                        <label><strong>Costo U.:</strong>  '.$detalle_ingreso['deting_costo'].'</label>
                    <br><br><br>

                    <table border="1" cellspacing="2" cellpadding="20">';
                        
                      $html = $html . '<tr>
                                <th align="left"><strong>Observaciones (Justificantes):</strong> '.$reg['ing_obs'].'</th>
                               
                                </tr>
                                <br><br><br><br><br><br>
                                _________________________ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________
                                <br>
                                <label></label>
                                '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'
                                <br>
                                <label>Responsable de Almacen</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Responsable Administrativo</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Jefe de Planta</label>
                            ';   

                    $htmltable = $html . '</table>';
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_almacen.pdf', 'I');

    }

    // REPORTE DE INGRESO MATERIA PRIMA
    public function reportePrimaReporte($id_envio)
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
            $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
            PDF::SetXY(120, 204);
            PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('Carta');
        //PDF::AddPage();

        // create some HTML content
   //     $carr = CarritoSolicitud::getListar();
        //$usr=Usuario::setNombreRegistro();
        $usr = Usuario::join('public._bp_personas as per','public._bp_usuarios.usr_prs_id','=','per.prs_id')
                ->where('usr_id',Auth::user()->usr_id)->first();
        $per=Collect($usr);
        //echo $per;
        $id=Auth::user()->usr_id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id)->first();
        $reg = Ingreso::join('acopio.envio_almacen as env','insumo.ingreso.ing_env_acop_id','=','env.enval_id')
                      ->where('ing_env_acop_id',$id_envio)->first();
        $detalle_ingreso = DetalleIngreso::where('deting_ing_id',$reg->ing_id)->first();
        // echo $reg['carr_ing_usr_id'];
    
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><img src="img/logopeqe.png" width="160" height="65"></th>
                             <th  width="400"><h3 align="center"><br>ALMACEN '.$planta['nombre_planta'].'<br>NOTA DE INGRESO<br>MATERIA PRIMA<br></h3></th>
                             <th  width="150"><h3 align="center"><br>Fecha: '.date('d/m/Y').'<br>Codigo No:'.$reg['ing_enumeracion'].'/'.date('Y',strtotime($reg['ing_registrado'])).'</h3></th>
                        </tr>
                    </table>
                    <br><br><br>
                        <label><strong>Responsable de Almacen</strong>: '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'</label>
                        <br><br>
                        <label><strong>Dependencia:</strong> '.$planta['nombre_planta'].'</label>
                        <br><br>
                        <label><strong>Insumo:</strong> LECHE </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Cantidad Total Enviado:</strong> '.$reg['enval_cant_total'].'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><strong>Cantidad Total Recibido:</strong> '.$detalle_ingreso['deting_cantidad'].'</label>
                        <br><br>
                        <label><strong>Costo U.:</strong>  '.$detalle_ingreso['deting_costo'].'</label>
                    <br><br><br>

                    <table border="1" cellspacing="2" cellpadding="20">';
                        
                      $html = $html . '<tr>
                                <th align="left"><strong>Observaciones (Justificantes):</strong> '.$reg['ing_obs'].'</th>
                               
                                </tr>
                                <br><br><br><br><br><br>
                                _________________________ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________
                                <br>
                                <label></label>
                                '. $per['prs_nombres'].' '.$per['prs_paterno'].' '.$per['prs_materno'].'
                                <br>
                                <label>Responsable de Almacen</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Responsable Administrativo</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Jefe de Planta</label>
                            ';   

                    $htmltable = $html . '</table>';
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('Reporte_almacen.pdf', 'I');

    }    
}
