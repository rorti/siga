<?php

namespace siga\Http\Controllers\insumo\insumo_recetas;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\insumo\insumo_registros\Insumo;
use siga\Modelo\insumo\insumo_registros\Datos;
use siga\Modelo\insumo\insumo_registros\Mercado;
use siga\Modelo\insumo\insumo_registros\UnidadMedida;
use siga\Modelo\insumo\insumo_recetas\Receta;
use siga\Modelo\insumo\insumo_recetas\DetalleReceta;
use siga\Modelo\insumo\insumo_registros\Sabor;
use siga\Modelo\insumo\insumo_registros\SubLinea;
use siga\Modelo\admin\Usuario;
use Yajra\Datatables\Datatables;
use DB;
use Auth;

class gbRecetasController extends Controller
{
    public function index()
    {
    	$listarInsumo = Insumo::getListarInsumo();
    	$listarUnidades = UnidadMedida::where('umed_estado','A')->get();
        $listarMercados = Mercado::where('mer_estado','A')->get();
        //$recetas = Receta::getListar();
        //dd($recetas);
    	$plantas = DB::table('public._bp_planta')->get();
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                            ->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        $id_linea_trabajo = Usuario::where('usr_id','=',Auth::user()->usr_id)->first();
        $ls_plantas = DB::table('public._bp_planta')->where('id_linea_trabajo','=',$id_linea_trabajo->usr_linea_trabajo)->get();
        //dd($ls_plantas);                    
    	$lineaTrabajo = DB::table('acopio.linea_trab')->get();
        return view('backend.administracion.insumo.insumo_recetas.index',compact('listarInsumo','listarUnidades','plantas','planta','lineaTrabajo','listarMercados','ls_plantas'));
    }

     public function create()
    {
        $receta = Receta::getListar();
        // dd($recetas);
        return Datatables::of($receta)->addColumn('acciones', function ($receta) {
            return '<button value="' . $receta->rec_id . '" class="btncirculo btn-xs btn-danger" onClick="Eliminar(this);"><i class="fa fa-trash-o"></i></button>';
        })
            ->editColumn('id', 'ID: {{$rec_id}}')
            ->make(true);   
    }

    public function store(Request $request)
    {   
        $this->validate(request(), [
            'nombre_receta'             => 'required',
            'cantidad_minima_receta'    => 'required',
            'unidad_base'               => 'required',
            'planta_receta'             => 'required',
            'produccion_receta'         => 'required|min:1',
            'mercado_receta'            => 'required|min:1',
            'rec_data'                  => 'required'
        ]);
        $arraycount = json_decode($request['rec_data']);
        if(count($arraycount)==0){
            dd("DATOS VACIOS");
        } 
        Receta::create([
            'rec_nombre'        => $request['nombre_receta'],
            'rec_cant_min'      => $request['cantidad_minima_receta'],
            'rec_uni_base'      => $request['unidad_base'],
            'rec_id_planta'     => $request['planta_receta'],
            'rec_id_lineatrab'  => $request['produccion_receta'],
            'rec_id_mercado'    => $request['mercado_receta'],
            'rec_data'          => $request['rec_data'],
            'rec_usr_id'        => Auth::user()->usr_id,
            'rec_registrado'    => '2018-12-18',
            'rec_modificado'    => '2018-12-18',
            'rec_estado'        => 'A'
        ]);
        return response()->json(['Mensaje' => 'Se registro correctamente']);
    }

    public function destroy($id)
    {
        $receta = Receta::getDestroy($id);
        return response()->json($receta);
    }

    public function nuevaReceta()
    {
        $sabor = Sabor::where('sab_estado','A')->get();
        $listarInsumo = Insumo::getListarInsumo();
        $listarUnidades = UnidadMedida::where('umed_estado','A')->get();
        $sublinea = SubLinea::where('sublin_estado','A')->get();
        //dd($sublinea);
        return view('backend.administracion.insumo.insumo_recetas.partials.formCreateReceta',compact('listarInsumo','listarUnidades','sabor','sublinea'));
    }

    public function registrarReceta(Request $request)
    {   
        if ($request['lineaProduccion'] == 1) {
            $detrec_id_ins_base = $request['descripcion_base'];
            $detrec_cantidad_base = $request['cantidad_base'];

            $detrec_id_ins_sab = $request['descripcion_saborizacion'];
            $detrec_cantidad_sab = $request['cantidad_saborizacion'];

            $detrec_id_ins_env = $request['descripcion_envase'];
            $detrec_cantidad_env = $request['cantidad_envase'];

            for ($i=0; $i <sizeof($detrec_id_ins_base) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_base[$i], "cantidad"=>$detrec_cantidad_base[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_sab) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_sab[$i], "cantidad"=>$detrec_cantidad_sab[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_env) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_env[$i], "cantidad"=>$detrec_cantidad_env[$i]); 
            }

        }elseif($request['lineaProduccion'] == 2){
            $detrec_id_ins_materia = $request['descripcion_materia'];
            $detrec_cantidad_materia = $request['cantidad_materia'];

            $detrec_id_ins_env = $request['descripcion_envase'];
            $detrec_cantidad_env = $request['cantidad_envase'];

            for ($i=0; $i <sizeof($detrec_id_ins_materia) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_materia[$i], "cantidad"=>$detrec_cantidad_materia[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_env) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_env[$i], "cantidad"=>$detrec_cantidad_env[$i]); 
            }
        }elseif($request['lineaProduccion'] == 3){
            $detrec_id_ins_materia = $request['descripcion_materia'];
            $detrec_cantidad_materia = $request['cantidad_materia'];
            
            $detrec_id_ins_env = $request['descripcion_envase'];
            $detrec_cantidad_env = $request['cantidad_envase'];
            for ($i=0; $i <sizeof($detrec_id_ins_materia) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_materia[$i], "cantidad"=>$detrec_cantidad_materia[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_env) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_env[$i], "cantidad"=>$detrec_cantidad_env[$i]); 
            }
        }elseif($request['lineaProduccion'] == 4){
            $detrec_id_ins_base = $request['descripcion_base'];
            $detrec_cantidad_base = $request['cantidad_base'];

            $detrec_id_ins_sab = $request['descripcion_saborizacion'];
            $detrec_cantidad_sab = $request['cantidad_saborizacion'];

            $detrec_id_ins_env = $request['descripcion_envase'];
            $detrec_cantidad_env = $request['cantidad_envase'];
            for ($i=0; $i <sizeof($detrec_id_ins_base) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_base[$i], "cantidad"=>$detrec_cantidad_base[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_sab) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_sab[$i], "cantidad"=>$detrec_cantidad_sab[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_env) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_env[$i], "cantidad"=>$detrec_cantidad_env[$i]); 
            }
        }elseif($request['lineaProduccion'] == 5){    
            $detrec_id_ins_base = $request['descripcion_base'];
            $detrec_cantidad_base = $request['cantidad_base'];

            $detrec_id_ins_sab = $request['descripcion_saborizacion'];
            $detrec_cantidad_sab = $request['cantidad_saborizacion'];

            $detrec_id_ins_env = $request['descripcion_envase'];
            $detrec_cantidad_env = $request['cantidad_envase'];
            for ($i=0; $i <sizeof($detrec_id_ins_base) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_base[$i], "cantidad"=>$detrec_cantidad_base[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_sab) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_sab[$i], "cantidad"=>$detrec_cantidad_sab[$i]); 
            }

            for ($i=0; $i <sizeof($detrec_id_ins_env) ; $i++) { 
                $detrecesta_datos[] = array("id_insumo"=>$detrec_id_ins_env[$i], "cantidad"=>$detrec_cantidad_env[$i]); 
            }
        }        
        //dd($detrecesta_datos);        
        $array_receta = array(
            'densidad'          => $request['densidad'],
            'vol_recipiente'    => $request['vol_recipiente'],
            'peso_mezcla'       => $request['peso_mezcla'],
            'peso_botella'      => $request['peso_botella'],
            'peso_tapa'         => $request['peso_tapa'],
            'solides_lie'       => $request['solides_lie'],
            'solides_lse'       => $request['solides_lse'],
            'acidez_lie'        => $request['acidez_lie'],
            'acidez_lse'        => $request['acidez_lse'],
            'ph_lie'            => $request['ph_lie'],
            'ph_lse'            => $request['ph_lse'],
            'viscosidad_lie'    => $request['viscosidad_lie'],
            'viscosidad_lse'    => $request['viscosidad_lse'],
            'densidad_lie'      => $request['densidad_lie'],
            'densidad_lse'      => $request['densidad_lse']
        );
        $datosjson = json_encode($array_receta);
        $receta = Receta::create([
            'rece_codigo'           => 'LACTO-001',
            'rece_enumeracion'      => 1,
            'rece_nombre'           => $request['nombre_receta'],
            'rece_lineaprod_id'     => $request['lineaProduccion'],
            'rece_sublinea_id'      => $request['sublinea'],
            'rece_sabor_id'         => $request['sabor'],
            'rece_presentacion'     => $request['presentacion'],
            'rece_uni_id'           => $request['unidad_medida'],
            'rece_prod_total'       => $request['peso_prod_total'],
            'rece_rendimiento_base' => $request['rendimiento_base'],
            'rece_datos_json'       => $datosjson,
            'rece_usr_id'           => Auth::user()->usr_id,

        ]);
        /*for ($i=0; $i <sizeof($detrecesta_datos); $i++) { 
            DetalleReceta::create([
                'detrece_id'    => $
            ]);
        }*/
        foreach ($detrecesta_datos as $det) {
            DetalleReceta::create([
                'detrece_rece_id'   => $receta->rece_id,
                'detrece_ins_id'    => $det['id_insumo'],
                'detrece_cantidad'  => $det['cantidad']
            ]);
        }
    }
}
