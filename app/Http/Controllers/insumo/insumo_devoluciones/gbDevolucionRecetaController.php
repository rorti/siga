<?php

namespace siga\Http\Controllers\insumo\insumo_devoluciones;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;

class gbDevolucionRecetaController extends Controller
{
    public function index()
    {
    	return view('backend.administracion.insumo.insumo_devolucion.devolucion_recibidas.index');
    }
}
