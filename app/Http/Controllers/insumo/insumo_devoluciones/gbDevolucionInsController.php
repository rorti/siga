<?php

namespace siga\Http\Controllers\insumo\insumo_devoluciones;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\insumo\insumo_solicitud\Aprobacion_Solicitud;
use siga\Modelo\insumo\insumo_devolucion\Devolucion;
use siga\Modelo\admin\Usuario;
use Yajra\Datatables\Datatables;
use Auth;
use DB;

class gbDevolucionInsController extends Controller
{
    public function index()
    {
    	return view('backend.administracion.insumo.insumo_devolucion.devolucion_insumos.index');
    }
   
     public function create()
    {
        $soldev = Aprobacion_Solicitud::join('insumo.tipo_solicitud as tip','insumo.aprobacion_solicitud.aprsol_tipo_solicitud','=','tip.tipsol_id')->where('aprsol_estado','=','A')->where('aprsol_usr_id','=',Auth::user()->usr_id)
            ->get();
        return Datatables::of($soldev)->addColumn('acciones', function ($soldev) {
            return '<button value="' . $soldev->aprsol_id . '" class="btn btn-success" onClick="MostrarEquipo(this);" data-toggle="modal" data-target="#myUpdateIns">V</button> <button value="' . $soldev->aprsol_id . '" class="btn btn-success" onClick="MostrarDevolucion(this);MostrarDataDev(this);" data-toggle="modal" data-target="#myCreateDevolucion">A</button>';
        })
            ->editColumn('id', 'ID: {{$aprsol_id}}')
            ->make(true);
    }

    public function listDetalleDevolucion($id)
    { 
        //echo $id;
        $listdet = Aprobacion_Solicitud::select('aprsol_id','aprsol_data')->where('aprsol_id', $id)->where('aprsol_usr_id','=',Auth::user()->usr_id)->first();
        $datas = json_decode($listdet->aprsol_data);
        $data2 =collect($datas);
         return Datatables::of($data2) ->addColumn('adicion', function ($adicion) {
           return '<input type="text" id="adicion1" name="row-1-age" value="0" size="3" placeholder="0.00">';
         })
          ->addColumn('devolucion', function ($devolucion) {
          return '<input type="text" id="devolucion" name="row-1-age" value="0" size="3" placeholder="0.00">';
         })
        //->editColumn('id', 'ID: {{$aprsol_id}}') 
            ->make(true);
    }

     public function store(Request $request)
    {
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        $id=$planta->id_planta;
        
        $num = Devolucion::join('public._bp_planta as plant', 'insumo.devolucion.dev_id_planta', '=', 'plant.id_planta')->select(DB::raw('MAX(dev_codnum) as nrodev'))->where('plant.id_planta', $id)->first();
        $cont=$num['nrodev'];
        $nid = $cont + 1;
        $fecha = date('Y');
        Devolucion::create([
            'dev_id_aprsol'     => $request['id_aprsol'],
            'dev_num_sal'       => $request['num_sal'],
            'dev_data'          => $request['data'],
            'dev_obs'           => $request['obs'],
            'dev_nom_rec'       => $request['nom_rec'],
            'dev_id_planta'     => $planta->id_planta,
            'dev_gestion'       => $fecha,
            'dev_codnum'        => $nid,
            'dev_usr_id'        =>  Auth::user()->usr_id,          
            'dev_estado'        => 'A',
           
        ]);
        return response()->json(['Mensaje' => 'Se registro correctamente']);
    }

     public function edit($id)
    {
        $devolucion = Aprobacion_Solicitud::setBuscar($id);
        return response()->json($devolucion);
    }
}
