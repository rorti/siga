<?php

namespace siga\Http\Controllers\insumo\insumo_devoluciones;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\insumo\insumo_devolucion\Devolucion;
use siga\Modelo\insumo\insumo_devolucion\Devolucion_Recibidas;
use siga\Modelo\insumo\Stock_Almacen;
use Yajra\Datatables\Datatables;
use siga\Modelo\admin\Usuario;  
use Auth;
use DB;

class gbDevolucionRecibidasController extends Controller
{
    public function index()
    {
    	return view('backend.administracion.insumo.insumo_devolucion.devolucion_recibidas.index');
    }

     public function create()
    {
        $rec = Devolucion::getListar();
        return Datatables::of($rec)->addColumn('acciones', function ($rec) {
            return '<button value="' . $rec->dev_id . '" class="btn btn-success" onClick="MostrarDatos(this);MostrarDevolucionRec(this);" data-toggle="modal" data-target="#myCreateRecibidas">V</button>';
        })
        ->addColumn('numdev', function ($numdev) {
            return $numdev->dev_codnum . ' / ' . $numdev->dev_gestion;
        })
            ->editColumn('id', 'ID: {{$dev_id}}')
            ->make(true);
    }

     public function edit($id)
    {
        $devolucion = Devolucion::setBuscar($id);
        return response()->json($devolucion);
    }

    public function listDetalleRecibidas($id)
    { 
        //echo $id;
        $listdet = Devolucion::select('dev_data')->where('dev_id', $id)->where('dev_usr_id','=',Auth::user()->usr_id)->first();
        $datas = json_decode($listdet->dev_data);
        $data2 =collect($datas);
         return Datatables::of($data2)
            ->make(true);
    }

     public function store(Request $request)
    {
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
    
        $id=$planta->id_planta;
        $num = Devolucion_Recibidas::join('public._bp_planta as plant', 'insumo.devolucion_recibidas.devrec_id_planta', '=', 'plant.id_planta')->select(DB::raw('MAX(devrec_cod_num) as nrodevrec'))->where('plant.id_planta', $id)->first();
        $cont=$num['nrodevrec'];
        $nid = $cont + 1;
        $fecha=date('Y');
        $ingreso_alm=Devolucion_Recibidas::create([
               // 'carr_ing_prov'    => $request['carr_ing_prov'],
                'devrec_id_dev'         => $request['devrec_id_dev'],
                'devrec_num_salida'     => $request['devrec_num_salida'],
                'devrec_nom_receta'     => $request['devrec_nom_receta'],
               // 'devrec_tipo_sol'       => $request['devrec_tipo_sol'],
                'devrec_data'           => $request['data'],
                'devrec_usr_id'         => Auth::user()->usr_id,
                'devrec_id_planta'      => $planta->id_planta,
                'devrec_cod_num'        => $nid,
                'devrec_gestion'        => $fecha,
                'devrec_estado'         => 'A',
            ]);
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                            ->where('usr_id','=',Auth::user()->usr_id)->first();
        $ins_devolucion =json_decode($request['data']);

        foreach ($ins_devolucion as $devolucion) {
        
           $stock1 = Stock_Almacen::where('stockal_ins_id','=' ,$devolucion->id1)->where('stockal_planta_id','=',$planta->id_planta)->first();
                      $id_ins = $stock1->stockal_ins_id;
                      $id_insumo = $devolucion->id1;
                      $id_plant = $planta->id_planta;
                      $cant_ingreso = $devolucion->devolucion1;
                      $stock_cantidad_ingreso = $stock1->stockal_cantidad + $cant_ingreso;
                      $stock1->stockal_cantidad = $stock_cantidad_ingreso;
                      $stock1->stockal_usr_id = Auth::user()->usr_id;
                      $stock1->save();                    
        }    
        return response()->json($ingreso_alm);
    }
}
