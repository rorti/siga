<?php

namespace siga\Http\Controllers\insumo\insumo_solicitudes;

use Illuminate\Http\Request;
use siga\Http\Controllers\Controller;
use siga\Modelo\insumo\insumo_solicitud\Solicitud;
use siga\Modelo\insumo\insumo_solicitud\Solicitud_Maquila;
use siga\Modelo\insumo\insumo_registros\Insumo;
use siga\Modelo\admin\Usuario;
use DB;
use Auth;
use Yajra\Datatables\Datatables;
use PDF;
use TCPDF;

class gbSolTraspasoController extends Controller
{
     public function index()
    {
    	$solMaquila = Solicitud::getlistarTraspaso();
    	$insumos = Insumo::getListarInsumo();
    	$plantas = DB::table('public._bp_planta')->get();
    	// dd($plantas);
    	return view('backend.administracion.insumo.insumo_solicitud.solicitud_traspaso.index', compact('solMaquila','insumos','plantas'));
    }

    public function create()
    {
    	$solMaquila = Solicitud::getlistarTraspaso();
    	return Datatables::of($solMaquila)->addColumn('acciones', function ($solMaquila) {
            if ($solMaquila->sol_estado == 'B') {
                return '<div class="text-center"><h4 class="text"><span class="label label-info">RECIBIDO</span></h4></div>';
            }
            return '<div class="text-center"><a href="boletaSolMaquila/' . $solMaquila->sol_id . '" class="btn btn-md btn-primary" target="_blank">Boleta <i class="fa fa-file"></i></a></div>';
        })
            ->editColumn('id', 'ID: {{$sol_id}}')
            -> addColumn('sol_estado', function ($solMaquila) {
                if($solMaquila->sol_estado=='A')
                { 
                    return '<h4 class="text"><span class="label label-warning">PENDIENTE</span></h4>'; 
                }elseif($solMaquila->aprsol_estado == 'A')
                {
                    return '<h4 class="text"><span class="label label-success">APROBADO</span></h4>';
                }elseif($solMaquila->aprsol_estado=='B')
                { 
                    return '<h4 class="text"><span class="label label-danger">RECHAZADO</span></h4>'; 
                }
                            
             }) 
            -> addColumn('nombresCompleto', function ($solMaquila) {
            return $solMaquila->prs_nombres . ' ' . $solMaquila->prs_paterno . ' ' . $solMaquila->prs_materno;
        })
            ->make(true);

    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'insumo'            => 'required',
            'cantidad_traspaso'=> 'required',
            'unidad_medida'     => 'required',
            'origen'            => 'required|min:1',
            'destino'           => 'required|min:1',
            'observaciones'     => 'required'
        ]);
        $planta = Usuario::join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                            ->select('planta.id_planta')
                            ->where('usr_id','=',Auth::user()->usr_id)->first();
        $cod_nro_maq = Solicitud::where('sol_id_planta','=',$planta->id_planta)
                            ->select(DB::raw('MAX(sol_codnum) as codigo_nro_maq'))->first();
        $nro_cod_maq = $cod_nro_maq['codigo_nro_maq'] + 1;
        $solMaquila = Solicitud::create([
            'sol_id_insmaq'         => $request['insumo'],
            'sol_cantidad'       => $request['cantidad_traspaso'],
            'sol_um'             => $request['unidad_medida'],
            'sol_id_origen'         => $request['origen'],
            'sol_id_destino'        => $request['destino'],
            'sol_obs'            => $request['observaciones'],
            'sol_usr_id'         => Auth::user()->usr_id,
            'sol_id_tipo'           => 3,
            // 'solmaq_registrado'     => '2018-12-12',
            // 'solmaq_modificado'     => '2018-12-18',
            'sol_estado'         => 'A',
            'sol_id_planta'      => $planta->id_planta,
            'sol_codnum'        => $nro_cod_maq,
            'sol_data'          => $request['solmaq_data'],
            'sol_gestion'        => date("Y")
        ]);
        return response()->json($solMaquila);
    }

    public function boletaSolMaquila($id)
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('EBA');
        $pdf->SetTitle('EBA');
        $pdf->SetSubject('INSUMOS');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          PDF::SetXY(125, 199);
            $pdf->Cell(70,6,utf8_decode('Responsable de Acopio - EBA'),0,1,'C',0);
            PDF::SetXY(120, 204);
            PDF::Cell(15,6,utf8_decode('Nombre:'),0,0,'C',0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage('Carta');

        $usuario = Usuario::join('public._bp_personas as persona','public._bp_usuarios.usr_prs_id','=','persona.prs_id')->where('usr_id','=',Auth::user()->usr_id)->first();
        $id_user =  Auth::user()->usr_id;
        $planta = Usuario::join('_bp_planta', '_bp_usuarios.usr_planta_id', '=', '_bp_planta.id_planta')
                 ->where('usr_id',$id_user)->first();
        $reg = Solicitud::join('public._bp_planta as planta','insumo.solicitud.sol_id_origen','=','planta.id_planta')
                    ->join('public._bp_planta as plant','insumo.solicitud.sol_id_destino','=','plant.id_planta')
                    ->join('insumo.insumo as ins','insumo.solicitud.sol_id_insmaq','=','ins.ins_id')
                    ->join('public._bp_usuarios as usu','insumo.solicitud.sol_usr_id','=','usu.usr_id')
                    ->join('public._bp_personas as persona','usu.usr_prs_id','=','persona.prs_id')
                    ->select('insumo.solicitud.sol_id','insumo.solicitud.sol_um','insumo.solicitud.sol_obs','insumo.solicitud.sol_cantidad','insumo.solicitud.sol_registrado','ins.ins_desc','planta.nombre_planta as planta_origen','plant.nombre_planta as planta_destino','persona.prs_nombres','persona.prs_paterno','persona.prs_materno','insumo.solicitud.sol_codnum','insumo.solicitud.sol_gestion')
                    ->where('sol_estado','=','A')->where('sol_id','=',$id)->where('sol_usr_id','=',Auth::user()->usr_id)->where('sol_id_tipo','=',3)
            ->first();
    
        
        // dd($reg);
        $html = '   <table border="1" cellspacing="0" cellpadding="1">
                        <tr>
                             <th align="center" width="150"><img src="/img/logopeqe.png" width="160" height="65"></th>
                             <th  width="370"><h3 align="center"><br>ALMACEN '.$planta['nombre_planta'].'<br>NOTA DE SOLICITUD POR TRAPASO/MAQUILA</h3></th>
                             <th  width="150"><h3 align="center"><br>Fecha: '.$reg['sol_registrado'].'<br>Codigo No: '.$reg['sol_codnum'].'/'.$reg['sol_gestion'].'</h3></th>
                        </tr>
                    </table>
                    <br><br><br>
                        <label><strong>Solicitante:</strong> '.$usuario->prs_nombres.' '.$usuario->prs_paterno.' '.$usuario->prs_materno.'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        <br><br>
                        <label><strong>Dependencia:</strong> '.$planta['nombre_planta'].'</label>
                        <br><br>
                        
                        <h3 align="center">'.$reg['rec_nombre'].'</h3>
                    <br><br><br>

                    <table border="1" cellspacing="0">
                     
                        <tr>
                            <th align="center" bgcolor="#3498DB" width="50"><strong>Nro</strong></th>
                            <th align="center" bgcolor="#3498DB" width="160"><strong>INSUMO</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>UNIDAD</strong></th>
                            <th align="center" bgcolor="#3498DB" width="70"><strong>Cantidad</strong></th>
                            <th align="center" bgcolor="#3498DB" width="158"><strong>origen</strong></th>
                            <th align="center" bgcolor="#3498DB" width="158"><strong>Detino</strong></th>
                        </tr> ';
                     
                        // $tot1=0;
                   
                            $html = $html . '<tr align="center" BGCOLOR="#f3f0ff">
                                    <td align="center">1</td>
                                    <td align="center">'.$reg['ins_desc'].'</td>
                                    <td align="center">'.$reg['sol_um'].'</td>
                                    <td align="center">'.$reg['sol_cantidad'].'</td>
                                    <td align="center">'.$reg['planta_origen'].'</td>
                                    <td align="center">'.$reg['planta_destino'].'</td>
                                  </tr>';  
                        
                       // echo $tot1;   
                     $html = $html . '</table>

                     <br><br>
                            <table border="1">
                                <tr>
                                    <th height="50"><strong> Observaciones:</strong> '.$reg['sol_obs'].'</th>
                                </tr>
                            </table>
                     <br><br><br><br><br><br>';

                      $html = $html . '
                                <table>
                            <tr>
                                <td align="left">Solicitado por: __________________________________</td>
                                
                                <td align="left">Autorizado por: __________________________________</td>
                            </tr>
                            <tr>
                                <td align="left">Firma</td>
                                
                                <td align="left">Firma</td>
                            </tr>
                            <tr>
                                <td align="left">Nombre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$usuario->prs_nombres.' '.$usuario->prs_paterno.' '.$usuario->prs_materno.'</td>
                                <td align="left">Nombre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________________________</td>
                            </tr>
                            
                            
                            </table>
                            ';   

                    $htmltable = $html;
        $pdf->writeHTML($htmltable, true, 0, true, 0);


        // reset pointer to the last page
          
        $pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('boletaSolMaquila-'.$reg['solmaq_id'].'.pdf', 'I');
    }
}
