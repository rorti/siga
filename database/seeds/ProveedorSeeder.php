<?php

use Illuminate\Database\Seeder;

class ProveedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('insumo.proveedor')->insert([
            'prov_nom' 	=>  'ACOPIO',
            'prov_dir' 	=>  'LUGAR EMPRESA EBA',
            'prov_tel'	=>	'2486325',
            'prov_nom_res'	=> 'EBA',
            'prov_ap_res'	=> 'EBA',
            'prov_am_res'	=> 'EBA',
            'prov_tel_res'	=> '1313545',
            'prov_obs'		=> 'EMPRESA BOLIVIANA DE ALIMENTOS Y DERIVADOS',
            'prov_usr_id'	=> 3,
            'prov_id_planta'=> 1,
        ]);
    }
}
