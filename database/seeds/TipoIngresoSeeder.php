<?php

use Illuminate\Database\Seeder;

class TipoIngresoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::table('insumo.tipo_ingreso')->insert([
            'ting_nombre' 	=>  'INGRESO MATERIA PRIMA',
            'ting_usr_id'	=>  3,
            'ting_registrado' => Carbon\Carbon::now(),
            'ting_modificado' => Carbon\Carbon::now(),
        ]);
        DB::table('insumo.tipo_ingreso')->insert([
            'ting_nombre' 	=>  'INGRESO NORMAL',
            'ting_usr_id'	=>  3,
            'ting_registrado' => Carbon\Carbon::now(),
            'ting_modificado' => Carbon\Carbon::now(),
        ]);
    }
}
