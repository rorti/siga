@extends('backend.template.app')
@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
            <div class="col-md-12">
                <div class="col-md-1">
                    <a type="button" class="btn btn-dark"  style="background: #000000;" href="{{ url('ReportesInsumoMenu') }}"><span class="fa fas fa-align-justify" style="background: #ffffff;"></span><h7 style="color:#ffffff">&nbsp;&nbsp;MENU</h7></a>
                </div>
                <div class="col-md-8">
                     <h4><label for="box-title">INGRESOS ALMACEN (REPORTES)</label></h4>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
        <li class="active" id="tabsolReceta">
          <a data-toggle="tab" href="#ingresoNormal" class="btn btn-primary">
              INGRESO
          </a>
        </li>
        <li id="tabsolInsumo">
            <a data-toggle="tab" href="#solInsumo" class="btn btn-warning">
                INGRESO MATERIA PRIMA
            </a>
        </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade in active" id="ingresoNormal">
       <div class="box">
                <div class="box-header with-border text-center">
                    <h3 class="box-title">
                        INGRESO
                    </h3>
                        
                </div>
            <div id="no-more-tables">
                <table class="table table-hover table-striped table-condensed cf" style="width: 100%" id="lts-ingresoNormales">
                    <thead class="cf">
                                <tr>
                                    <th>
                                        Nro
                                    </th>                                    
                                    <th>
                                        Reporte
                                    </th>
                                    <th>
                                        Nro. Ingreso
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Usuario Registro
                                    </th>
                                    <th>
                                        Nro. Remisión
                                    </th>                    
                                </tr>
                            </thead>
                            <tbody>
            
                            </tbody>
                            <tfoot>
                                <tr>
                                   <th>
                                        Nro
                                    </th>                                    
                                    <th>
                                        Reporte
                                    </th>
                                    <th>
                                        Nro. Ingreso
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Usuario Registro
                                    </th>
                                    <th>
                                        Nro. Remisión
                                    </th>                                   
                                </tr>
                            </tfoot>
                </table>
            </div>
        </div>
        </div>
        <div class="tab-pane fade" id="solInsumo">
            <div class="box">
                <div class="box-header with-border text-center">
                    <h3 class="box-title">
                        LISTADO DE SOLICITUDES POR ADICIÓN DE INSUMOS
                    </h3>
                </div>
                <div class="box-body">
                 <div id="no-more-tables">
                    <table class="table table-hover table-striped table-condensed cf" style="width: 100%" id="lts-solporInsumo">
                        <thead class="cf">
                                <tr>
                                    <th>
                                        N°
                                    </th>                                    
                                    <th>
                                        Opc.
                                    </th>
                                    <th>
                                        N° Solicitud
                                    </th>
                                    <th>
                                        Nota Salida
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Receta
                                    </th>
                                    <th>
                                        Solicitante
                                    </th>
                                    <th>
                                        Estado
                                    </th>     
                        
                                </tr>
                            </thead>
                            <tbody>
            
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>
                                        N°
                                    </th>                                    
                                    <th>
                                        Opc.
                                    </th>
                                    <th>
                                        N° Solicitud
                                    </th>
                                    <th>
                                        Nota Salida
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Receta
                                    </th>
                                    <th>
                                        Solicitante
                                    </th>
                                    <th>
                                        Estado
                                    </th>     
                                  
                                </tr>
                            </tfoot>
                    </table>
                </div>
                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>

    </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    var t = $('#lts-ingresoNormales').DataTable( {
      
         "processing": true,
            "serverSide": true,
            "ajax": "createListarIngresoAlmacen",
            "columns":[
                {data: 'carr_ing_id'},
                {data: 'acciones',orderable: false, searchable: false},
                {data: 'carr_ing_num'}, 
                {data: 'carr_ing_fech'},
                {data: 'nombre_usuario'},
                {data: 'carr_ing_rem'},

        ],
        
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]],
         
       
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
</script>
@endpush  


