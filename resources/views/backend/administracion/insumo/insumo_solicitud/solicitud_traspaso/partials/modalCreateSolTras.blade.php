<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myCreateSolTraspaso" tabindex="-5">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    REGISTRO NUEVA SOLICITUD TRAPASO/MAQUILA
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                        {!! Form::open(['id'=>'proveedor', 'files' => true])!!}
                        <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                            <input id="id" name="provid" type="hidden" value="">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Insumo:
                                                </label>
                                                <select name="solmaq_insumo" id="solmaq_insumo" class="form-control">
                                                    <option>Seleccione insumo</option>
                                                    @foreach($insumos as $insu)
                                                        <option value="{{ $insu->ins_codigo.'+'.$insu->ins_desc.'+'.$insu->ins_id }}">{{ $insu->ins_codigo.' - '.$insu->ins_desc }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Cant:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::number('solmaq_cant', null, array('placeholder' => 'Cantidad','class' => 'form-control','id'=>'solmaq_cant')) !!}
                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">                                                       
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    UM:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('solmaq_unidad', null, array('placeholder' => 'Unidad de medida','class' => 'form-control','id'=>'solmaq_unidad')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Origen:
                                                </label>
                                                <select name="solmaq_origen" id="solmaq_origen" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    @foreach($plantas as $plan)
                                                        <option value="{{ $plan->id_planta }}">{{ $plan->nombre_planta }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Destino:
                                                </label>
                                                <select name="solmaq_destino" id="solmaq_destino" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    @foreach($plantas as $plan)
                                                        <option value="{{ $plan->id_planta }}">{{ $plan->nombre_planta }}</option>
                                                    @endforeach
                                                </select> 
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>
                                                Obseraciones:
                                            </label>
                                            <textarea class="form-control" name="solmaq_obs" id="solmaq_obs"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>
                                                STOCK ACTUAL EN ALMACEN DEL INSUMO:
                                            </label>
                                            <span class="block input-icon input-icon-right" style="background: red;">
                                                <input class="form-control" name="stock_cantidad_actual" id="stock_cantidad_actual" style="background: white; border-color: red;" placeholder="AQUI APARECERA LA CANTIDAD DISPONIBLE EN ALMACEN DEL INSUMO SELECCIONADO"></input>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <br>                                
                            </input>
                        </input>
                    </hr>
                </div>
            </div>       
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                {!!link_to('#',$title='Enviar Solicitud', $attributes=['id'=>'registroSolTrasp','class'=>'btn btn-success','style'=>''], $secure=null)!!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $("#solmaq_insumo").on('change', function(e){
        var solmaq_id = e.target.value;
        var codIns = solmaq_id.split("+")[0];
        var descIns = solmaq_id.split("+")[1];
        var idIns = solmaq_id.split("+")[2];
        console.log("EL ID INSUMO ES: "+idIns);

        $.ajax({
            type: 'get',
            url: "StockActual/"+idIns,
            dataType: 'json',
            async:false,
            success: function(data)
            {
                console.log("CANTIDAD ACTUAL EN STOCK: "+data.stockal_cantidad);
                $("#stock_cantidad_actual").val(data.stockal_cantidad);
                $("#stock_cantidad_actual").css("background","yellow");
            }
        });
    });
</script>
@endpush