@extends('backend.template.app')
@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="container col-lg-12" style="background: white;">        
            <?php $now = new DateTime('America/La_Paz'); ?>
            <div class="text-center">
                <h3 style="color:#2067b4"><strong>REGISTRO RECETA EBA</strong></h3> 
            </div>                             
        
            <form action="{{ url('RegistrarReceta') }}" class="form-horizontal" method="GET">
                <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                <input id="fecha_resgistro" name="fecha_resgistro" type="hidden" value="<?php echo $now->format('d-m-Y H:i:s'); ?>">
                <input type="hidden" name="nro_acopio" id="nro_acopio" value="">
                    <div class="row"> 
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Nombre Producto:
                                    </label>                                   
                                        {!! Form::text('nombre_receta', null, array('placeholder' => 'Nombre del producto','class' => 'form-control','id'=>'nombre_receta')) !!}                                       
                                </div>
                            </div>
                        </div>                                
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Linea Producción:
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <select class="form-control" name="lineaProduccion" id="lineaProduccion">
                                            <option value="0">Seleccione</option>
                                            <option value="1">Lacteos</option>
                                            <option value="2">Almendra</option>
                                            <option value="3">Miel</option>
                                            <option value="4">Frutos</option>
                                            <option value="5">Derivados</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Sublinea
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <select class="form-control" id="sublinea" name="sublinea">
                                            <option value="0">Seleccione</option>
                                            @foreach($sublinea as $sub)
                                            <option value="{{$sub->sublin_id}}">{{$sub->sublin_nombre}}</option>
                                            @endforeach
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Sabor
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <select class="form-control" id="sabor" name="sabor">
                                            <option value="0">Seleccione</option>
                                            @foreach($sabor as $sab)                                            
                                                <option value="{{$sab->sab_id}}">{{$sab->sab_nombre}}</option>
                                            @endforeach
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Presentación
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('presentacion', null, array('placeholder' => 'Presentación','class' => 'form-control','id'=>'presentacion')) !!}                                       
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Unidad Medida
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <select class="form-control" id="unidad_medida" name="unidad_medida">
                                            <option value="0">Seleccione</option>
                                            @foreach($listarUnidades as $uni)                                                
                                                <option value="{{$uni->umed_id}}">{{$uni->umed_nombre}}</option>                                                
                                            @endforeach
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Peso Producto Total
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('peso_prod_total', null, array('placeholder' => 'Peso Producto Total ','class' => 'form-control','id'=>'peso_prod_total')) !!}                                       </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Rendimiento Base
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('rendimiento_base', null, array('placeholder' => 'Rendimiento Base','class' => 'form-control','id'=>'rendimiento_base')) !!}                                       </span>
                                </div>
                            </div>
                        </div>                                       
                    </div>
                <div id="OcultarMateriaPrima" style="display: none">
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>MATERIA PRIMA</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="">
                                            <tr class="tr-header">
                                                <th>Descripcion</th>
                                                <th>Unidad</th>
                                                <th>Cantidad</th>
                                            </tr>    
                                            <tr class="items_columsReceta2">
                                                <td><select name="descripcion_materia[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarInsumo as $insumo)
                                                            <option value="{{$insumo->ins_id}}">{{ $insumo->ins_codigo.' - '.$insumo->ins_desc}}</option>
                                                        @endforeach
                                                    </select>
                                                <td><select name="unidad_materia[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarUnidades as $unidad)
                                                            <option value="{{$unidad->umed_id}}">{{$unidad->umed_nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name="cantidad_materia[]" class="form-control"></td>
                                            </tr>
                                        </table> 

                                    </div>
                        </div>
                    </div>
                </div>
                <div id="OcultaCaracEnv" style="display:none">    
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>CARACTERISTICAS DE ENVASE</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Densidad
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('densidad', null, array('placeholder' => 'Densidad','class' => 'form-control','id'=>'densidad')) !!}                                       </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Volumen del Recipiente
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('vol_recipiente', null, array('placeholder' => 'Rendimiento Base','class' => 'form-control','id'=>'vol_recipiente')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Peso Mezcla
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('peso_mezcla', null, array('placeholder' => 'Rendimiento Base','class' => 'form-control','id'=>'peso_mezcla')) !!}                                       </span>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Peso Botella
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('peso_botella', null, array('placeholder' => 'Rendimiento Base','class' => 'form-control','id'=>'peso_botella')) !!}                                       </span>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Peso Tapa
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('peso_tapa', null, array('placeholder' => 'Rendimiento Base','class' => 'form-control','id'=>'peso_tapa')) !!}                                       </span>
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>
                <div id="OcultarformulacionBase" style="display: none">
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>FORMULACION DE LA BASE</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <div class="">
                                    <a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More Person"><span class="btn btn-primary">Añadir Insumo</span>
                                    </a>
                                </div>
                                    </div>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="tb">
                                            <tr class="tr-header">
                                                <th>Descripcion</th>
                                                <th>Unidad</th>
                                                <th>Cantidad</th>
                                                <th>Opcion</th>
                                                <!-- <th>Rango Adicional</th>                                             -->
                                            <tr class="items_columsReceta2">
                                                <td><select name="descripcion_base[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarInsumo as $insumo)
                                                            <option value="{{$insumo->ins_id}}">{{ $insumo->ins_codigo.' - '.$insumo->ins_desc}}</option>
                                                        @endforeach
                                                    </select>
                                                <td><select name="unidad_base[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarUnidades as $unidad)
                                                            <option value="{{$unidad->umed_id}}">{{$unidad->umed_nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name="cantidad_base[]" class="form-control"></td>
                                                <td><div class="text-center"><a href='javascript:void(0);'  class='remove btncirculo btn-md btn-danger'><i class="glyphicon glyphicon-remove"></i></a></div></td>
                                                <!-- <td><input type="text" name="rango[]" class="form-control"></td> -->
                                            </tr>
                                        </table> 

                                    </div>
                        </div>
                    </div>
                </div>
                <div id="OcultarSaborizacion" style="display: none">
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>SABORIZACIÓN</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                    <a href="javascript:void(0);" style="font-size:18px;" id="addMoreSabor" title="Add More Person"><span class="btn btn-primary">Añadir Insumo</span>
                                    </a>
                                    </div>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="tbSabor">
                                            <tr class="tr-header">
                                                <th>Descripcion</th>
                                                <th>Unidad</th>
                                                <th>Cantidad</th>
                                                <th>Opcion</th>
                                                <!-- <th>Rango Adicional</th>                                             -->
                                            <tr class="items_columsReceta2">
                                                <td><select name="descripcion_saborizacion[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarInsumo as $insumo)
                                                            <option value="{{$insumo->ins_id}}">{{ $insumo->ins_codigo.' - '.$insumo->ins_desc}}</option>
                                                        @endforeach
                                                    </select>
                                                <td><select name="unidad_saborizacion[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarUnidades as $unidad)
                                                            <option value="{{$unidad->umed_id}}">{{$unidad->umed_nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name="cantidad_saborizacion[]" class="form-control"></td>
                                                <td><div class="text-center"><a href='javascript:void(0);'  class='removeSabor btncirculo btn-md btn-danger'><i class="glyphicon glyphicon-remove"></i></a></div></td>
                                            </tr>
                                        </table>   

                                    </div>
                        </div>
                    </div>
                </div>
                <div id="OcultarMatEnv" style="display: none">
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>MATERIAL DE ENVASADO</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                    <a href="javascript:void(0);" style="font-size:18px;" id="addMoreEnvase" title="Add More Person"><span class="btn btn-primary">Añadir Envase</span>
                                    </a>
                                    </div>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="tbEnvase">
                                            <tr class="tr-header">
                                                <th>Descripcion</th>
                                                <th>Unidad</th>
                                                <th>Cantidad</th>
                                                <th>Opcion</th>
                                                <!-- <th>Rango Adicional</th>                                             -->
                                            <tr class="items_columsReceta2">
                                                <td><select name="descripcion_envase[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarInsumo as $insumo)
                                                            <option value="{{$insumo->ins_id}}">{{ $insumo->ins_codigo.' - '.$insumo->ins_desc}}</option>
                                                        @endforeach
                                                    </select>
                                                <td><select name="unidad_envase[]" class="form-control">
                                                        <!--<option value="">Seleccione</option>-->
                                                        @foreach($listarUnidades as $unidad)
                                                            <option value="{{$unidad->umed_id}}">{{$unidad->umed_nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name="cantidad_envase[]" class="form-control"></td>
                                                <td><div class="text-center"><a href='javascript:void(0);'  class='removeEnvase btncirculo btn-md btn-danger'><i class="glyphicon glyphicon-remove"></i></a></div></td>
                                            </tr>
                                        </table>   

                                    </div>
                        </div>
                    </div>
                </div>
                <div id="ocultaParFisQui" style="display: none;">
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>PARAMETROS FISICO QUÍMICO</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label></label>
                                    <div class="text-right">
                                        SOLIDES (%): 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                       LIE
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('solides_lie', null, array('placeholder' => '','class' => 'form-control','id'=>'solides_lie')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        LSE
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('solides_lse', null, array('placeholder' => '','class' => 'form-control','id'=>'solides_lse')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label></label>
                                    <div class="text-right">
                                        ACIDEZ (%AI): 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                       
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('acidez_lie', null, array('placeholder' => '','class' => 'form-control','id'=>'acidez_lie')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                    
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('acidez_lse', null, array('placeholder' => '','class' => 'form-control','id'=>'acidez_lse')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label></label>
                                    <div class="text-right">
                                        PH (-): 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                       
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('ph_lie', null, array('placeholder' => '','class' => 'form-control','id'=>'ph_lie')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                    
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('ph_lse', null, array('placeholder' => '','class' => 'form-control','id'=>'ph_lse')) !!}                                       </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label></label>
                                    <div class="text-right">
                                        VISCOSIDAD (Seg) a 10°C: 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                       
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('viscosidad_lie', null, array('placeholder' => '','class' => 'form-control','id'=>'viscosidad_lie')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                    
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('viscosidad_lse', null, array('placeholder' => '','class' => 'form-control','id'=>'viscosidad_lse')) !!}                                       </span>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label></label>
                                    <div class="text-right">
                                        DENSIDAD 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                       
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('densidad_lie', null, array('placeholder' => '','class' => 'form-control','id'=>'densidad_lie')) !!}                                       </span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                    
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        {!! Form::text('densidad_lse', null, array('placeholder' => '','class' => 'form-control','id'=>'densidad_lse')) !!}                                       </span>
                                </div>
                            </div>
                        </div>   
                           
                    </div>
                </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                            <a class="btn btn-danger btn-lg" href="{{ url('InsumoRecetas') }}" type="button">
                            Cerrar
                            </a>
                            <!-- {!!link_to('#',$title='Registrar', $attributes=['id'=>'registro','class'=>'btn btn-success'], $secure=null)!!} -->
                            <input type="submit"  value="Registrar" class="btn btn-success btn-lg">
                            </div>
                        </div>
                    </div>
                
            </form>
            
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script>
$(function(){
    $('#lineaProduccion').change(function(){
        if($(this).val()==1){
            $('#OcultarMateriaPrima').hide();
            $('#OcultaCaracEnv').show();
            $('#OcultarformulacionBase').show();
            $('#OcultarSaborizacion').show();
            $('#OcultarMatEnv').show();
            $('#ocultaParFisQui').show();
        }else if($(this).val()==2){
            $('#OcultarMateriaPrima').show();
            $('#OcultaCaracEnv').hide();
            $('#OcultarformulacionBase').hide();
            $('#OcultarSaborizacion').hide();
            $('#OcultarMatEnv').show();
            $('#ocultaParFisQui').hide();
        }else if($(this).val()==3){
            $('#OcultarMateriaPrima').show();
            $('#OcultaCaracEnv').hide();
            $('#OcultarformulacionBase').hide();
            $('#OcultarSaborizacion').hide();
            $('#OcultarMatEnv').show();
            $('#ocultaParFisQui').hide();
        }else if($(this).val()==4){
            $('#OcultarMateriaPrima').hide();
            $('#OcultaCaracEnv').show();
            $('#OcultarformulacionBase').show();
            $('#OcultarSaborizacion').show();
            $('#OcultarMatEnv').show();
            $('#ocultaParFisQui').hide();
        }else if($(this).val()==5){
            $('#OcultarMateriaPrima').hide();
            $('#OcultaCaracEnv').show();
            $('#OcultarformulacionBase').show();
            $('#OcultarSaborizacion').show();
            $('#OcultarMatEnv').show();
            $('#ocultaParFisQui').hide();
        }
  
    })
});

 $('#addMore').on('click', function() {
              var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
              data.find("input").val('');
     });
     $(document).on('click', '.remove', function() {
         var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
             $(this).closest("tr").remove();
           } else {
             swal('Lo siento','No puede borrar el unico item');
           }
      });

$('#addMoreSabor').on('click', function() {
              var data = $("#tbSabor tr:eq(1)").clone(true).appendTo("#tbSabor");
              data.find("input").val('');
     });
     $(document).on('click', '.removeSabor', function() {
         var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
             $(this).closest("tr").remove();
           } else {
             swal('Lo siento','No puede borrar el unico item');
           }
      });
$('#addMoreEnvase').on('click', function() {
              var data = $("#tbEnvase tr:eq(1)").clone(true).appendTo("#tbEnvase");
              data.find("input").val('');
     });
     $(document).on('click', '.removeEnvase', function() {
         var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
             $(this).closest("tr").remove();
           } else {
             swal('Lo siento','No puede borrar el unico item');
           }
      });
</script>
@endpush
